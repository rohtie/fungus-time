import random
from camera import *
from room import *
from pygame import *
import pygame
import glob
from settings import *


class GameMap:
    """
    This class contains and generates the game map, which is a two-dimensional list of rooms.
    It is also responsible for generating a minimap.
    """

    def __init__(self):
        # Names to images containing roomdata
        self.roomData = list(zip(sorted(glob.glob('./assets/maps/map*.png')), sorted(glob.glob('./assets/maps/tile*.png'))))

        # Datastructures for containing the rooms (Max 64 rooms)
        self.roomMap = [[0 for x in range(8)] for x in range(8)]
        self.rooms = []

        self.generate()

        self.current = self.rooms[0]

        self.changed = True

    # Generate a random map
    def generate(self):
        """
        Generate a random map by adding rooms in random directions.
        """

        # Place start room
        x, y = random.randint(0, 7), random.randint(0, 7)

        r = Room(x, y)

        self.roomMap[x][y] = r
        self.rooms.append(r)

        tres = False
        bos = False

        # Generate random amount of rooms
        numRooms = random.randint(8, 18)
        for i in range(0, numRooms):
            # Add trasure room
            if i == numRooms-1:
                tres = True
            else:
                tres = False

            placed = False
            while not placed:
                placed = False

                # Pick random room
                current = self.rooms[random.randint(0, len(self.rooms)-1)]

                x, y = current.x, current.y

                # make a room in random direction
                pos = random.randint(0, 4)

                # Make room down
                if pos == 0:
                    if self.addRoom(x, y+1, tres, bos):
                        placed = True

                # Make room left
                elif pos == 1:
                    if self.addRoom(x-1, y, tres, bos):
                        placed = True

                # Make room up
                elif pos == 2:
                    if self.addRoom(x, y-1, tres, bos):
                        placed = True

                # Make room right
                else:
                    if self.addRoom(x+ 1, y, tres, bos):
                        placed = True

            # Add boss room
            if i == numRooms-2:
                self.rooms.reverse()
                self.rooms[0].boss()
                self.rooms.reverse()

        # Go through rooms, generate blocks/tiles and close openings that lead nowhere
        for room in self.rooms:
            x, y = room.x, room.y
            w, h = 34, 26

            # Check up
            if y-1 < 0 or self.roomMap[x][y-1] == 0:
                room.addEntity (w/2 - 1, 0, Platform )
                room.addEntity (w/2, 0, Platform )
                room.openings[0] = False

            # Add door
            else:
                room.addEntity (w/2 - 1, 0, topDoor )

                # Fix room switching bug
                room.addEntity (w/2 - 2, -1, Platform )
                room.addEntity (w/2 + 1, -1, Platform )

            # Check right
            if x+1 > 7 or self.roomMap[x+1][y] == 0:
                room.addEntity (w -1, h/2 - 1, Platform )
                room.addEntity (w - 1, h/2, Platform )
                room.openings[1] = False

            # Add door
            else:
                room.addEntity (w -1, h/2 - 1, rightDoor )

                # Fix room switching bug
                room.addEntity (w, h/2 + 1, Platform )

            # Check down
            if y+1 > 7 or self.roomMap[x][y+1] == 0:
                room.addEntity (w/2 - 1, h -1, Platform )
                room.addEntity (w/2, h -1, Platform )
                room.openings[2] = False

            # Add door
            else:
                room.addEntity (w/2 - 1, h -1, bottomDoor )

                # Fix room switching bug
                room.addEntity (w/2 - 1, h - 1, Step )
                room.addEntity (w/2, h - 1, Step )

                room.addEntity (w/2 - 2, h, Platform )
                room.addEntity (w/2 + 1, h, Platform )

            # Check left
            if x-1 < 0 or self.roomMap[x-1][y] == 0:
                room.addEntity (0, h/2 - 1, Platform )
                room.addEntity (0, h/2, Platform )
                room.openings[3] = False

            # Add door
            else:
                room.addEntity (0, h/2 - 1, leftDoor )

                # Fix room switching bug
                room.addEntity (0 - 1, h/2 + 1, Platform )

            # Generate random room
            roomD = self.roomData[random.randint(0, len(self.roomData) - 1)]
            room.generate(0, 0, roomD[0])
            room.genTiles(roomD[1])

        # Starting room should not have any enemies
        for enemy in self.rooms[0].enemies:
            enemy.life = -1

    # Add a room at given position of given type
    def addRoom(self, x, y, tres, bos):
        """
        Attempt to add a room to the map.
        """

        # Check if out of bounds
        if x < 0 or y < 0 or x == 8 or y == 8:
            return False

        # Check if already a room
        if isinstance(self.roomMap[x][y], Room):
            return False

        # Check if neighbours are empty
        surrRooms = []
        if x+1 < 8:
            if self.roomMap[x+1][y] != 0:
                surrRooms.append(self.roomMap[x+1][y])

        if y+1 < 8:
            if self.roomMap[x][y+1] != 0:
                surrRooms.append(self.roomMap[x][y+1])

        if x-1 > 0:
            if self.roomMap[x-1][y] != 0:
                surrRooms.append(self.roomMap[x-1][y])

        if y-1 > 0:
            if self.roomMap[x][y-1] != 0:
                surrRooms.append(self.roomMap[x][y-1])

        # Only place room if there is 1 surrounding room
        r = Room(x, y)
        if len(surrRooms) > 1:
            return False

        # If trasure room request, check if neighbour is bossroom
        if tres:
            for room in surrRooms:
                if room.bos:
                    return False

            r.treasure()

        self.roomMap[x][y] = r
        self.rooms.append(r)

        return True

    # Return the starting room
    def startRoom(self):
        """
        Returns the starting room.

        Used at the start of a new game to place the player.
        """
        return self.rooms[0]

    # Return the current room
    def currentRoom(self, player):
        """
        Returns the current room.

        Used every frame to know which room the player is in.
        """

        self.changed = False

        x, y = self.current.cords

        px, py = player.rect.topleft

        # Left
        if px < 0:
            player.rect.x = 34*size
            self.current = self.roomMap[x-1][y]
            self.changed = True

        # Right
        elif px > 34*size:
            player.rect.x = 0
            self.current = self.roomMap[x+1][y]
            self.changed = True

        # Up
        elif py < 0:
            player.rect.y = 26*size
            self.current = self.roomMap[x][y-1]
            self.changed = True

        # Down
        elif py > 26*size:
            player.rect.y = 0
            self.current = self.roomMap[x][y+1]
            self.changed = True

        return self.current

    # Return generated minimap
    def miniMap(self, currentRoom):
        """
        Generates and returns the minimap.
        """

        # Update minimap
        if ((closeDoors and (currentRoom.locked == True or currentRoom.finished)) or not closeDoors) and not currentRoom.visited:
            up, right, down, left = currentRoom.openings
            x, y = currentRoom.cords

            if up:
                self.roomMap[x][y-1].seen = True
            if right:
                self.roomMap[x+1][y].seen = True
            if down:
                self.roomMap[x][y+1].seen = True
            if left:
                self.roomMap[x-1][y].seen = True

            currentRoom.visited = True

        # Draw minimap
        minimap = Surface((12*8*sizeMultiplier, 8*8*sizeMultiplier))
        minimap = minimap.convert()
        minimap.fill(Color("#FFFF00"))
        minimap.set_colorkey(Color("#FFFF00"))

        for room in self.rooms:
            # Current room
            if room == currentRoom:
                im = Surface((12*sizeMultiplier, 8*sizeMultiplier))
                im.convert()
                im.fill(Color("#BBBBBB"))

                minimap.blit(im, room.rect)

            # Already visited room
            elif room.visited:
                im = Surface((12*sizeMultiplier, 8*sizeMultiplier))
                im.convert()
                im.fill(Color("#777777"))

                minimap.blit(im, room.rect)

            # Room that has been seen, but not visited
            elif room.seen:
                minimap.blit(room.image, room.rect)

        return minimap

    # Reset map, empty all lists and set all to zero
    def new(self):
        """
        Reset the map and generate a new one.
        """

        self.roomMap = [[0 for x in range(8)] for x in range(8)]
        self.rooms = []

        self.generate()

        self.current = self.rooms[0]
