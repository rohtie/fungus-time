import pygame
from pygame import *
import glob
from PIL import Image
import sys
import os

tSize = (32, 25)
pSize = (32, 24)

sys.path.insert(0, os.path.abspath('./../assets/maps/'))


class Tile(pygame.sprite.Sprite):
    """Represents a tile and contains all relevant
       tile information for the leveleditor."""

    def __init__(self, tilesheet, tx, ty, x, y, index):
        pygame.sprite.Sprite.__init__(self)

        # Set to correct tile
        self.image = Surface((32, 32))
        self.image.blit(tilesheet, (-tx, -ty))
        self.image.set_colorkey(Color("#071210"))

        self.aImage = self.image.copy()
        self.aImage.set_alpha(96)

        self.rect = Rect(x, y, 32, 32)

        # Set tileindex
        self.index = index


class Room:
    """Represents a room contains all relevant room
       information for the leveleditor."""

    def __init__(self, pName, tName):
        # Name
        self.pName = pName
        self.tName = tName

        # Load index map
        self.platformIndex = self.generateIndexMap("platform_index.png")
        self.tileIndex = self.generateIndexMap("forest_tiles_map.png")

        # Load tilesheet
        self.platformSheet = pygame.image.load(
            sys.path[0] + "/../sprites/platform_tiles.png").convert()

        self.tileSheet = pygame.image.load(
            sys.path[0] + "/../sprites/forest_tiles.png").convert()

        # Tile list data
        self.platformImage = Image.open(pName)
        self.platformData = self.platformImage.load()
        self.tileImage = Image.open(tName)
        self.tileData = self.tileImage.load()

        # Generate tile list
        self.platforms = self.generateTiles(
            self.platformData, self.platformIndex, self.platformSheet, pSize)

        self.tiles = self.generateTiles(
            self.tileData, self.tileIndex, self.tileSheet, tSize)

        self.doorTiles = []
        self.tileDoorways(self.tileSheet, self.tileIndex)

    def generateIndexMap(self, name):
        """Generate and return a dict which can be used to convert
           between pixel indices to tilesheet coordinates."""

        indexMap = {}

        # Load image
        im = Image.open(sys.path[0] + "/" + name)
        data = im.load()

        # Parse
        width, height = im.size
        for y in range(height):
            for x in range(width):
                indexMap[data[x, y]] = (x*32, y*32)

        return indexMap

    def generateTiles(self, data, indexMap, tilesheet, size):
        """
        Go through image data and convert it to tile objects.
        """

        tileList = []

        # Generate room based on image data
        w, h = size
        for y in range(0, h):
            for x in range(0, w):
                if data[x, y] != 0:
                    tx, ty = indexMap[data[x, y]]
                    tileList.append(
                        Tile(tilesheet, tx, ty, 512 + x*32, y*32, data[x, y])
                    )

        return tileList

    def save(self):
        """
        Save all changes made to this room.
        """

        self.save_file(
            self.platformImage,
            self.platformData,
            self.platforms,
            self.pName,
            pSize
        )

        self.save_file(
            self.tileImage,
            self.tileData,
            self.tiles,
            self.tName,
            tSize
        )

    def save_file(self, fileObj, data, tiles, name, size):
        """
        Generalized save function for tile and platform maps.
        """

        w, h = size

        # Zero out old map
        for y in range(0, h):
            for x in range(0, w):
                data[x, y] = 0

        # Add new data
        for tile in tiles:
            data[(tile.rect.x - 512)/32, tile.rect.y/32] = tile.index

        fileObj.save(name)

    # Add tiles around doorways
    def tileDoorways(self, tilesheet, indexMap):
        """
        Generate tiles around the doorways.
        """

        # get correct tiles
        upWall = indexMap[54]
        downLeaf = indexMap[34]
        downMiddleLeaf = indexMap[17]

        rightSlope = indexMap[53]
        rightWall = indexMap[4]
        leftSlope = indexMap[55]
        leftWall = indexMap[10]

        leftStump = indexMap[42]
        rightStump = indexMap[44]
        leftPreLeaf = indexMap[33]
        rightPreLeaf = indexMap[35]
        leftMidLeaf = indexMap[16]
        rightMidLeaf = indexMap[18]

        rightLeaf = indexMap[2]
        leftLeaf = indexMap[40]

        # Add tiles at correct place

        # Up
        self.addTile(tilesheet, rightSlope, 17, 0)
        self.addTile(tilesheet, leftSlope, 14, 0)

        # Down
        self.addTile(tilesheet, leftStump, 15, 24)
        self.addTile(tilesheet, rightStump, 16, 24)
        self.addTile(tilesheet, leftPreLeaf, 17, 23)
        self.addTile(tilesheet, rightPreLeaf, 14, 23)
        self.addTile(tilesheet, leftMidLeaf, 17, 24)
        self.addTile(tilesheet, rightMidLeaf, 14, 24)

        # Left
        self.addTile(tilesheet, leftWall, 0, 10)
        self.addTile(tilesheet, rightLeaf, 0, 13)

        # Right
        self.addTile(tilesheet, rightWall, 31, 10)
        self.addTile(tilesheet, leftLeaf, 31, 13)

    # Add a tile with given coordinates
    def addTile(self, tilesheet, tileCords, x, y):
        """
        Generate tiles around the doorways.
        """

        tx, ty = tileCords
        self.doorTiles.append(Tile(tilesheet, tx, ty, 512 + x*32, y*32, 0))


def generatePreview(tiles, tileIndexMap, tileIndex, size):
    """
    Generate a preview of what the image file will look like
    """

    w, h = size

    result = Surface(size)
    for tile in tiles:
        x, y = tile.rect.topleft
        tx, ty = tileIndex[tile.index]

        pixel = Surface((1, 1))
        pixel.blit(tileIndexMap, (-(tx/32), -(ty/32)))

        result.blit(pixel, (x/32 - 16, y/32))

    result = pygame.transform.scale(result, (w*8, h*8))

    return result


def update(tiles, tilesheet, tileIndex, leftClick, rightClick, selpos):
    """
    Handle editing input from user.
    """

    if leftClick:
        x, y = pygame.mouse.get_pos()
        sx, sy = (int(x/32), int(y/32))

        # Insert new tile
        if x > 512:
            # Remove existing
            for tile in tiles:
                if tile.rect.collidepoint(pygame.mouse.get_pos()):
                    tiles.remove(tile)

            # Find index
            curIndex = 0
            tx, ty = selpos
            indexpos = selpos
            for index, pos in tileIndex.iteritems():
                if indexpos == pos:
                    curIndex = index
                    break

            # Add new
            if curIndex != 0:
                tiles.append(Tile(tilesheet, tx, ty, sx*32, sy*32, curIndex))

        # Select a tile
        else:
            selpos = (sx*32, sy*32)
            return selpos

    elif rightClick:
        for tile in tiles:
            if tile.rect.collidepoint(pygame.mouse.get_pos()):
                tiles.remove(tile)

    return selpos


def tilePick(tiles, tileIndex, selpos):
    """
    return the coordinates of the requested tile on the tilesheet.
    """

    # Get tile of given type
    for tile in tiles:
        if tile.rect.collidepoint(pygame.mouse.get_pos()):
            if tile.index != 0:
                return tileIndex[tile.index]
            break

    return selpos


def main():
    """
    Main leveleditor function. Called upon starting the leveleditor.
    """

    # Init pygame
    pygame.init()
    # pygame.font.init()

    # Init display
    screen = display.set_mode((512 + 32 * 32, 32 * 25), 0, 16)

    # Background
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((12, 12, 12))

    # Selection
    select = Surface((32, 32))
    select.fill(Color("#FFFFFF"))
    hole = Surface((30, 30))
    hole.fill(Color("#FF00FF"))
    select.blit(hole, (1, 1))
    select.set_colorkey(Color("#FF00FF"))
    selpos = (0, 0)

    timer = time.Clock()

    # Booleans
    showPlatforms = showTiles = tileMode = True
    leftClick = rightClick = alphaPlatforms = leftAlt = False

    # Load rooms
    rooms = []

    for pName, tName in zip(sorted(glob.glob(sys.path[0] + '/' + 'map*.png')), sorted(glob.glob(sys.path[0] + '/' + 'tile*.png'))):
        rooms.append(Room(pName, tName))

    # Add treasure and boss rooms
    rooms.append(Room(sys.path[0] + '/' + "treasure.png", sys.path[0] + '/' + "treasure_tile.png"))
    rooms.append(Room(sys.path[0] + '/' + "boss.png", sys.path[0] + '/' + "boss_tile.png"))

    platformSheet = pygame.image.load(sys.path[0] + "/../sprites/platform_tiles.png").convert()
    platformSheet.set_colorkey(Color("#071210"))
    tileSheet = pygame.image.load(sys.path[0] + "/../sprites/forest_tiles.png").convert()
    tileSheet.set_colorkey(Color("#071210"))

    roomIndex = 0
    curRoom = None

    while 1:
        timer.tick()

        # Handle keyevents
        for e in pygame.event.get():
            # Exit
            if e.type == QUIT or (e.type == KEYDOWN and e.key == K_ESCAPE):
                raise SystemExit("QUIT")

            # Switches
            if e.type == KEYDOWN:

                # Switch between tile mode and platform mode
                if e.key == K_z:
                    tileMode = not tileMode

                # Toggle platforms
                if e.key == K_x:
                    showPlatforms = not showPlatforms

                # Toggle tiles
                if e.key == K_c:
                    showTiles = not showTiles

                # Toggle transparent platforms
                if e.key == K_v:
                    alphaPlatforms = not alphaPlatforms

                # Save room
                if e.key == K_s:
                    curRoom.save()

                # New room
                if e.key == K_n:
                    # Set new index
                    roomIndex = len(rooms) - 2

                    # Generate new room based on current room
                    # PName, tName = curRoom.pName, curRoom.tName

                    # Generate new empty room
                    curRoom = Room(sys.path[0] + '/empty_map.png', sys.path[0] + '/empty_tile.png')

                    # Insert it after the last normal room
                    rooms.insert(roomIndex, curRoom)

                    # Change image names so that it saved as a new map
                    mapNum = '{0:03}'.format(roomIndex + 1)

                    curRoom.tName = sys.path[0] + '/' + 'tile' + mapNum + '.png'
                    curRoom.pName = sys.path[0] + '/' + 'map' + mapNum + '.png'

                # Decrement room
                if e.key == K_LEFT:
                    if roomIndex > 0:
                        roomIndex -= 1
                    else:
                        roomIndex = len(rooms) - 1

                # Increment room
                if e.key == K_RIGHT:
                    if roomIndex < len(rooms) - 1:
                        roomIndex += 1
                    else:
                        roomIndex = 0

                if e.key == K_LALT:
                    leftAlt = True

            if e.type == KEYUP:
                if e.key == K_LALT:
                    leftAlt = False

            if e.type == MOUSEBUTTONDOWN:
                if e.button == 1:
                    leftClick = True

                elif e.button == 3:
                    rightClick = True

            if e.type == MOUSEBUTTONUP:
                if e.button == 1:
                    leftClick = False

                elif e.button == 3:
                    rightClick = False

        # Get current room
        curRoom = rooms[roomIndex]

        tiles = curRoom.tiles
        platforms = curRoom.platforms

        tileIndex = curRoom.tileIndex
        platformIndex = curRoom.platformIndex

        # Bg
        screen.blit(background, (0, 0))

        # Pick a tile
        if leftAlt and leftClick:
            if tileMode:
                selpos = tilePick(tiles, tileIndex, selpos)
            else:
                selpos = tilePick(platforms, platformIndex, selpos)

        # Update UI
        if tileMode:
            screen.blit(tileSheet, (0, 0))
            selpos = update(tiles, curRoom.tileSheet, tileIndex, leftClick, rightClick, selpos)
        else:
            screen.blit(platformSheet, (0, 0))
            selpos = update(platforms, curRoom.platformSheet, platformIndex, leftClick, rightClick, selpos)

        # Previews
        screen.blit(generatePreview(platforms, pygame.image.load(sys.path[0] + '/' + "platform_index.png").convert(), platformIndex, pSize), (0, 512))
        screen.blit(generatePreview(tiles, pygame.image.load(sys.path[0] + '/' + "forest_tiles_map.png").convert(), tileIndex, tSize), (32*8, 512))

        # Draw selection box
        screen.blit(select, selpos)

        # Draw tiles
        if showTiles:
            for tile in tiles:
                screen.blit(tile.image, tile.rect.topleft)

            for tile in curRoom.doorTiles:
                screen.blit(tile.image, tile.rect.topleft)

        # Draw platforms
        if showPlatforms:
            if alphaPlatforms:
                for platform in platforms:
                    screen.blit(platform.aImage, platform.rect.topleft)

            else:
                for platform in platforms:
                    screen.blit(platform.image, platform.rect.topleft)

        # Separator
        separator = Surface((1, 32*24))
        separator.fill(Color("#AAAAAA"))
        screen.blit(separator, (511, 0))

        pygame.display.flip()

if(__name__ == "__main__"):
    main()
