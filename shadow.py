import pygame
from pygame import *
import random
from macro import *


class Shadow(pygame.sprite.Sprite):
    """
    Class for handling shadows
    """

    def __init__(self, shadowType):
        pygame.sprite.Sprite.__init__(self)

        self.image = loadSprite(shadowType, False)
        self.image.set_alpha(150)

        self.rect = self.image.get_rect()

    def update(self, entity, solidObjects):
        """
        Update position of shadow
        """

        self.rect.topleft = entity.rect.bottomleft
        startY = self.rect.y

        # Move rectangle down until it hits a object or max distance
        while self.rect.y - startY < 128:
            self.rect.y += 63

            for solid in solidObjects:
                if self.rect.colliderect(solid):
                    self.rect.bottom = solid.rect.top
                    self.rect.y += 2
                    self.image.set_alpha(150 - (self.rect.y - startY)*0.75)
                    return

        self.rect.y += 2
        self.image.set_alpha(0)