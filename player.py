import pygame
from pygame import *
from settings import *
from platform import *
from consumable import *
from item import *
from unlockable import *
from anim import *
from bullet import *
from room import *
from shadow import *
import random

from math import radians as deg2rad

# Height/width of player
w = 24*sizeMultiplier
h = 48*sizeMultiplier

dw = 24*sizeMultiplier
dh = 32*sizeMultiplier



class Player(pygame.sprite.Sprite):
    """
    This class controls the player
    """

    def __init__(self, x, y, startingRoom):
        pygame.sprite.Sprite.__init__(self)
        self.xvel = 0
        self.yvel = 0

        # Player states
        self.onGround = False
        self.faceright = True
        self.holdingJump = False
        self.hit = False
        self.holdingTime = 0
        self.duck = False
        self.holdingItem = None
        self.attack = False
        self.walking = False
        self.pass_through_step = False
        self.inAir = False
        self.inPortal = False
        self.damaged = False

        self.hitCounter = 0
        self.damageDuration = 0

        # Load animations
        self.walk = Animation("charlie_trombone_walk_final", "#000000", damage_filter=createDamageFeedback)
        self.walk_attack = Animation("charlie_walk_trombone_shoot", "#000000", damage_filter=createDamageFeedback)
        self.idle_attack = Animation("charlie_trombone_idle_shoot", "#000000", damage_filter=createDamageFeedback)

        self.duck_walk = Animation("charlie_trombone_duck_walk", "#000000", damage_filter=createDamageFeedback)
        self.duck_walk_attack = Animation("charlie_trombone_duck_shoot_walk", "#000000", damage_filter=createDamageFeedback)
        self.duck_idle_attack = Animation("charlie_trombone_duck_idle_shoot", "#000000", damage_filter=createDamageFeedback)

        self.shootJumpUp = Animation("charlie_jump_up_shoot", "#000000", damage_filter=createDamageFeedback)
        self.shootJumpDown = Animation("charlie_jump_down_shoot", "#000000", damage_filter=createDamageFeedback)

        # Load idle animations
        self.idle = Animation("charlie_trombone_idle_stand", "#000000", damage_filter=createDamageFeedback)
        self.idleDuck = Animation("charlie_trombone_duck_idle_stand", "#000000", damage_filter=createDamageFeedback)
        self.idleHolding = Animation("charlie_trombone_idle_holding", "#000000", damage_filter=createDamageFeedback)
        self.idleJump = Animation("charlie_trombone_idle_jump", "#000000", damage_filter=createDamageFeedback)
        self.idleJumpUp = Animation("charlie_jump_up_idle", "#000000", damage_filter=createDamageFeedback)
        self.idleJumpDown = Animation("charlie_jump_down_idle", "#000000", damage_filter=createDamageFeedback)

        # Set default image
        self.image = self.idle.next(self.faceright, self.damaged)

        # Shadow
        self.shadow = Shadow("cons_shadow")

        # Setup collision boxes
        self.rect = Rect(0, 0, w, h)
        self.standrect = self.rect
        self.sitrect = Rect(0, 0, dw, dh)

        self.rect.left += 1 * size
        self.rect.top += 11 * size

        # Player stats
        self.lives = 4      # Number of heart containers - max 16
        self.life = 4       # How much life is left
        self.keys = 0       # Number of keys
        self.money = 0      # Amount of money

        # Player weapon stats
        self.damage = 1
        self.speed = 10
        self.rate = 20
        self.rateCount = 0
        self.range = 400

        self.message = None

        # Items that can be taken
        self.items = [subItem for subItem in Item.__subclasses__()] # Get all subclasses of Item
        random.shuffle(self.items)

        # Dirty rect
        self.oldPlayerDirty = Rect(0, 0, 0, 0)

    def update(self, crouch, left, right, attack, jump, pass_through_step, room):
        """
        Compute the next state and position of the player based on input from keyboard.
        """

        self.pass_through_step = pass_through_step

        # Player is holding item above head, freeze controls
        if self.holdingItem:
            if self.holdingItem.stillHolding(room):
                crouch = left = right = attack = jump = pass_through_step = False
                self.xvel = 0
            else:
                self.holdingItem = None

        # Player got hit -> temporary invincible
        if self.hit and self.hitCounter < 100:
            self.hitCounter += 1
            self.damageDuration += 1
        else:
            self.hit = False
            self.hitCounter = 0
            self.damageDuration = 0

        self.damaged = self.hit and self.damageDuration < 6

        # Make player duck
        if crouch and not self.duck:
            self.sitrect.topleft = self.rect.topleft
            self.rect = self.sitrect
            self.rect.top += h-dh
            self.duck = True

        # Try standing up
        elif not crouch and self.duck:

            # Assume player can stand up
            self.standrect.topleft = self.rect.topleft
            self.rect = self.standrect
            self.rect.top -= h-dh
            self.duck = False

            # Check if standing up works
            for p in room.platforms:
                if sprite.collide_rect(self, p):
                    self.sitrect.topleft = self.rect.topleft
                    self.rect = self.sitrect
                    self.rect.top += h-dh
                    self.duck = True
                    break

        self.crouch = crouch

        # Jump
        if jump:
            if self.onGround:
                self.holdingJump = True

            if self.holdingJump:
                self.holdingTime += 1

                self.yvel = -10 - self.holdingTime

                if self.holdingTime > 7:
                    self.yvel = -17

                if self.holdingTime > 13:
                    self.holdingJump = False
        else:
            self.holdingJump = False
            self.holdingTime = 0


        # Move left
        if left:
            self.xvel -= 0.25*sizeMultiplier

            if self.xvel < -5*sizeMultiplier:
                self.xvel = -5*sizeMultiplier

            self.faceright = False

        # Move right
        if right:
            self.xvel += 0.25*sizeMultiplier

            if self.xvel > 5*sizeMultiplier:
                self.xvel = 5*sizeMultiplier

            self.faceright = True

        # Player is falling, apply gravitation
        if not self.onGround:
            self.yvel += 0.4*sizeMultiplier

            if self.yvel > 25*sizeMultiplier:
                self.yvel = 25*sizeMultiplier

        # Emulate friction
        if not(left or right):
            if self.xvel > 0:
                self.xvel -= 0.125*sizeMultiplier

            if self.xvel < 0:
                self.xvel += 0.125*sizeMultiplier

        # Check x-axis collisions
        self.rect.left += self.xvel
        self.collideXvel(self.xvel, room)

        # Check y-axis collisions
        self.rect.top += self.yvel
        self.onGround = False
        self.collideYvel(self.yvel, room)

        self.dynamicCollide(room)

        # Attack
        if attack:
            self.attack = True

            # Control firerate
            if self.rateCount > self.rate:
                self.rateCount = 0

            # Create bullet at player position
            if self.rateCount == 0:
                # Find out where bullets should spawn
                if self.faceright:
                    x, y = self.rect.topright
                    direction = 0
                else:
                    x, y = self.rect.topleft
                    direction = 180

                room.bullets.append(PlayerBullet(x, y + 5, deg2rad(direction), self))

        self.rateCount += 1

        # Update shadow
        self.shadow.update(self, room.platforms + room.doors + room.steps)

    def draw(self, screen, camera):
        """
        Draw player sprite

        Returns dirty rectangle
        """

        # Draw shadow
        screen.blit(self.shadow.image, camera.apply(self.shadow))

        # Check if we are walking
        if self.xvel == 0:
            self.walking = False
        else:
            self.walking = True

        # Check if player is in air
        if self.yvel < 0:
            self.inAir = True
        else:
            self.inAir = False

        # Find out which frame of which animation to show
        if self.walking:
            if self.duck:
                # Sync walking
                self.duck_walk.advance()
                self.duck_walk_attack.advance()

                if self.attack:
                    self.image = self.duck_walk_attack.current(self.faceright, self.damaged)
                    self.attack = False

                else:
                    self.image = self.duck_walk.current(self.faceright, self.damaged)
            else:
                # Sync walking
                self.walk.advance()
                self.walk_attack.advance()

                if self.attack:
                    if self.yvel < -0.1 or self.yvel > 0.8:
                        if self.yvel > 0:
                            self.image = self.shootJumpDown.next(self.faceright, self.damaged)
                        else:
                            self.image = self.shootJumpUp.next(self.faceright, self.damaged)

                    else:
                        self.image = self.walk_attack.current(self.faceright, self.damaged)

                    self.attack = False

                else:
                    if self.yvel < -0.1 or self.yvel > 0.8:
                        if self.yvel > 0:
                            self.image = self.idleJumpDown.next(self.faceright, self.damaged)
                        else:
                            self.image = self.idleJumpUp.next(self.faceright, self.damaged)

                    else:
                        self.image = self.walk.current(self.faceright, self.damaged)

        # Idle graphics
        else:
            if self.duck:
                self.duck_walk.reset()
                self.duck_walk_attack.reset()

                if self.attack:
                    self.image = self.duck_idle_attack.next(self.faceright, self.damaged)
                    self.attack = False

                else:
                    self.image = self.idleDuck.next(self.faceright, self.damaged)

            else:
                self.walk.reset()
                self.walk_attack.reset()

                if self.attack:
                    if self.yvel < -0.1 or self.yvel > 0.8:
                        if self.yvel > 0:
                            self.image = self.shootJumpDown.next(self.faceright, self.damaged)
                        else:
                            self.image = self.shootJumpUp.next(self.faceright, self.damaged)
                    else:
                        self.image = self.idle_attack.next(self.faceright, self.damaged)

                    self.attack = False

                else:
                    if self.yvel < -0.1 or self.yvel > 0.8:
                        if self.yvel > 0:
                            self.image = self.idleJumpDown.next(self.faceright, self.damaged)
                        else:
                            self.image = self.idleJumpUp.next(self.faceright, self.damaged)

                    else:
                        self.image = self.idle.next(self.faceright, self.damaged)

        if self.holdingItem:
            self.image = self.idleHolding.next(self.faceright, self.damaged)
            # if self.faceright:
            #     self.image = self.idleHoldingRight
            # else:
            #     self.image = self.idleHoldingLeft

        # Draw player
        nx, ny = camera.apply(self).topleft
        if self.faceright:
            nx -= 13*sizeMultiplier
        else:
            nx -= 31*sizeMultiplier

        screen.blit(self.image, (nx, ny))


        # Draw message
        if self.holdingItem:
            screen.blit(self.message.image, (sw - 500, sh - 32))

        # Handle dirty rectangles
        dirty = []

        if dirtOpt:
            playerDirty = Rect(nx, ny, 64*2, 48*2)
            dirty = [self.oldPlayerDirty, playerDirty]
            self.oldPlayerDirty = playerDirty

        return dirty


    def collideXvel(self, xvel, room):
        """
        Check collision with static objects on x axis
        """
        if room.locked:
            solids = room.platforms + room.doors
        else:
            solids = room.platforms

        solids = solids + room.steps

        for p in solids:
            if sprite.collide_rect(self, p):
                if not isinstance(p, Step):

                    # Collision right
                    if xvel > 0:
                        self.rect.right = p.rect.left
                        self.xvel -= 0.25

                    # Collision left
                    if xvel < 0:
                        self.rect.left = p.rect.right
                        self.xvel += 0.25

    def collideYvel(self, yvel, room):
        """
        Check collision with static objects on y axis
        """

        if room.locked:
            solids = room.platforms + room.doors
        else:
            solids = room.platforms

        solids = solids + room.steps

        for p in solids:
            # Check collision with steps
            if isinstance(p, Step):
                # Make a small collision box at bottom of player
                x, y = self.rect.midbottom
                stepCol = Rect(x, y, 1, h/4)
                stepCol.y -= h/4

                # Land on step
                if yvel > 0 and stepCol.colliderect(p) and not self.pass_through_step:
                    self.rect.bottom = p.rect.top
                    self.onGround = True
                    self.yvel = 0

                    # Fall damage
                    if yvel >= 15*sizeMultiplier:
                        self.hit = True
                        self.life -= 0.5

            # Check collisions with other platforms
            elif sprite.collide_rect(self, p):

                    # Collision top
                    if yvel < 0:
                        self.rect.top = p.rect.bottom
                        self.yvel = 0
                        self.holdingJump = False

                    # Collision bottom
                    if yvel > 0:
                        self.rect.bottom = p.rect.top
                        self.onGround = True
                        self.yvel = 0

                        # Fall damage
                        if yvel >= 15*sizeMultiplier:
                            self.hit = True
                            self.life -= 0.5

    def dynamicCollide(self, room):
        """
        check collisions with game objects.
        """

        # Check collision with consumables (hearts, keys, etc)
        for c in room.consumables:
            if sprite.collide_rect(self, c):

                # Key
                if isinstance(c, Key):
                    self.keys += 1
                    room.shadows.remove(c.shadow)
                    room.consumables.remove(c)

                # Key
                if isinstance(c, Money):
                    self.money += 1
                    room.shadows.remove(c.shadow)
                    room.consumables.remove(c)

                # Heart
                if isinstance(c, Heart):
                    lifeDiff = self.lives - self.life

                    # Make sure lifepoints is <= heartcontainers
                    if lifeDiff >= 0.5:
                        if lifeDiff == 0.5:
                            self.life += 0.5
                        else:
                            self.life += 1

                        room.shadows.remove(c.shadow)
                        room.consumables.remove(c)

        # Check collision with chests
        for u in room.unlockables:
            if sprite.collide_rect(self, u):
                # Chests
                if isinstance(u, Chest) and not isinstance(u, LockedChest) and u.locked:
                    u.unlock(self.items, room)

                elif isinstance(u, LockedChest) and self.keys > 0  and u.locked:
                    self.keys -= 1
                    u.unlock(self.items, room)

        # Check collision with items
        for i in room.items:
            if sprite.collide_rect(self, i):

                # Apply powerup to player
                i.apply(self)
                self.holdingItem = i
                self.message = i.getMessage()

        # Check collision with enemies
        for e in room.enemies:
            if not e.isDead() and not self.hit:
                if sprite.collide_mask(self, e):
                    if not self.hit:
                        if self.faceright:
                            self.xvel = -5*sizeMultiplier
                        else:
                            self.xvel = 5*sizeMultiplier

                        self.life -= 0.5
                        self.hit = True

        # Check collision with hazards
        for h in room.hazards:
            if sprite.collide_rect(self, h):
                if not self.hit:
                    self.life -= 0.5
                    self.hit = True

        # Check collision with portal
        if room.portal:
            if sprite.collide_rect(self, room.portal):
                self.inPortal = True
