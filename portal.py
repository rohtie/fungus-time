import pygame
from pygame import *
from settings import *
from macro import *


class Portal(pygame.sprite.Sprite):
    """
    Portal will transport the player to the next level
    """

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)

        # Sprite
        self.image = loadSprite("portal", True, Color("#000000"))
        self.rect = self.image.get_rect(center=(x, y))

    def update(self):
        """
        Slowly move portal down to boss stage.
        """

        if self.rect.top != 650:
            self.rect.top += 1