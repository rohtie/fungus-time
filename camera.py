# Import pygame
from pygame import *
from settings import *
from macro import *
from queue import Queue

DISPLAY = (sw, sh - 120)


class Camera(object):
    """
    This class takes care of side scrolling
    """

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.position = Rect(0, 120, width, height)

        self.dirty = False
        self.oldDirty = []
        self.newDirty = []

        self.xvel = 0
        self.yvel = 0

        self.q = Queue()
        self.queue = False
        self.smoothCamera = False

    def apply(self, target):
        """
        Move target sprite so that is looks like we moved the viewport
        """

        mtarget = target.rect.move(self.position.topleft)

        return mtarget

    def applyTile(self, tile):
        """
        Specialized apply function for tiles
        Tracks dirty tiles
        """

        mtile = tile.rect.move(self.position.topleft)

        # If camera moved, append dirty rectangles
        if self.dirty:
            if onScreen(mtile):
                self.newDirty.append(mtile)

        return mtile

    def update(self, player, smoothCamera = True):
        """
        Calculate how much objects need to be moved
        """

        self.dirty = False

        w, h = self.width, self.height

        # Get player position
        x, y = player.rect.bottomleft

        # Get camera position where player is in center
        x, y = x - DISPLAY[0]/2, y - DISPLAY[1]/2

        # Get old position
        oldx, oldy = self.position.topleft
        oldx, oldy = -oldx, (-oldy)+120

        """
        Calculate camera velocity for a smooth experience
        """

        if self.smoothCamera:
            if self.queue:
                # Queue based
                self.q.put((x, y))

                if self.q.qsize() == 10:
                    x, y = self.q.get()

            else:
                # slow, but usable
                speed = 0.5
                brakespeed = 0.25
                brakeBoundary = 1
                maxspeed = 5

            # Calculate velocity
            if x == oldx:
                self.xvel = 0
            if y == oldy:
                self.yvel = 0

            if x < oldx:
                self.xvel -= speed

            elif x > oldx:
                self.xvel += speed

            if y < oldy:
                self.yvel -= speed

            elif y > oldy:
                self.yvel += speed

            # Brake

            if player.xvel == 0:
                if self.xvel > brakeBoundary:
                    self.xvel -= brakespeed

                elif self.xvel < -brakeBoundary:
                    self.xvel += brakespeed

                else:
                    self.xvel = 0

            if player.yvel == 0:
                if self.yvel > brakeBoundary:
                    self.yvel -= brakespeed

                elif self.yvel < -brakeBoundary:
                    self.yvel += brakespeed

                else:
                    self.yvel = 0


            # Restrain velocity on x axis
            if self.xvel > maxspeed:
                self.xvel = maxspeed

            elif self.xvel < -maxspeed:
                self.xvel = -maxspeed

            # Restrain velocity on y axis
            if self.yvel > maxspeed:
                self.yvel = maxspeed

            elif self.yvel < -maxspeed:
                self.yvel = -maxspeed

            # Apply velocity
            oldx += self.xvel
            oldy += self.yvel

            x, y = oldx, oldy

        """
        Make sure camera position doesn't go beyond boundaries
        """

        # Left edge
        if x < 0:
            x = 0

        # Right edge
        if x > w - DISPLAY[0]:
            x = w - DISPLAY[0]

        # Up edge
        if y < 0:
            y = 0

        # Down edge
        if y > h - DISPLAY[1]:
            y = h - DISPLAY[1]

        """
        Dirty rectangles
        """
        if dirtOpt:

            oldx, oldy = self.position.topleft
            newx, newy = (x, y+120)

            # Camera difference
            xdiff = abs(abs(oldx) - abs(newx))
            ydiff = abs(abs(oldy) - abs(newy))

            # Player velocity
            px = abs (int(player.xvel) )
            py = abs (int(player.yvel) )

            # Find out if camera moved enough to update
            if (xdiff >= px and xdiff > 0) or (ydiff >= py and ydiff > 0):
                self.position = Rect(-x, -y+120, w, h)

                # Tiles are dirty, update rectangles
                self.oldDirty = self.newDirty
                self.newDirty = []
                self.dirty = True

        # Not dirty
        else:

            # Invert x and y in order to calculate how much to shift the opposite way
            self.position = Rect(-x, -y+120, w, h)