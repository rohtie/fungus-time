import pygame
from pygame import *
from PIL import Image
import random

from player import Player
from camera import *
from ui import *
from room import *
from gamemap import *
from settings import *
from macro import *


class Game:
    """
    Game class contains all game related objects
    """

    def __init__(self):
        """
        Initalize game objects
        """

        # Init game objects
        self.gameMap = GameMap()
        self.currentRoom = self.gameMap.startRoom()
        self.player = Player(8*size, size, self.currentRoom)
        self.ui = UI(DISPLAY[0], 120)
        self.camera = Camera(size*34, size*26)

        # Init keys
        self.crouch = self.left = self.right = self.attack = self.jump = self.pass_through_step = False

        # Check double tap down
        self.pass_through_step_time = 0

        # Dirty rect
        self.oldPlayerDirty = Rect(0, 0, 0, 0)

        self.bossMusic = False
        self.credits = False

    def handleKey(self, e):
        """
        Handle keyevents
        """

        # Press a key
        if e.type == KEYDOWN:
            if e.key == K_z:
                self.jump = True

            if e.key == K_x:
                self.attack = True

            if e.key == K_DOWN:
                self.crouch = True

            if e.key == K_LEFT:
                self.left = True
            if e.key == K_RIGHT:
                self.right = True

            # DEBUG KEYS
            if e.key == K_p:
                # Killal enemies
                for enemy in self.currentRoom.enemies:
                    enemy.life = -1

            if e.key == K_t:
                # Teleport to boss room
                self.currentRoom = self.gameMap.rooms[len(self.gameMap.rooms)-1]
                self.gameMap.current = self.currentRoom

            if e.key == K_b:
                # Teleport to treasure room
                self.currentRoom = self.gameMap.rooms[len(self.gameMap.rooms)-2]
                self.gameMap.current = self.currentRoom


        # Release a key
        if e.type == KEYUP:
            if e.key == K_z:
                self.jump = False

            if e.key == K_x:
                self.attack = False

            if e.key == K_DOWN:
                self.crouch = False

            if e.key == K_LEFT:
                self.left = False
            if e.key == K_RIGHT:
                self.right = False


    def update(self):
        """
        Update game objects

        Returns true if player is still alive
        """

        # Handle passing through steps
        if self.crouch and self.jump:
            self.pass_through_step_time = 14
            self.pass_through_step = True
            self.jump = False

        if self.pass_through_step_time < -1:
            self.pass_through_step = False

        else:
            self.jump = False
            self.pass_through_step_time -= 1

        # update player
        self.player.update(self.crouch, self.left, self.right, self.attack, self.jump, self.pass_through_step, self.currentRoom)

        # Update current room
        self.currentRoom = self.gameMap.currentRoom(self.player)
        self.currentRoom.update(self.player)
        self.currentRoom.check()

        # Move camera accordingly
        self.camera.update(self.player, not self.gameMap.changed)

        # Sync ui
        self.ui.update(self.player)

        # Play boss music
        if self.currentRoom.bos and not self.currentRoom.finished and not self.bossMusic:
            playMusic("DST-FinalMetropolis")

            self.bossMusic = True

        elif self.currentRoom.finished and self.bossMusic:
            playMusic("DST-DarkSeraphim")

            self.bossMusic = False

        # Check if player went to next level
        if self.player.inPortal:
            # Start credits
            self.credits = True
            return False

            # TODO: If more than one boss, generate new map:

            # Generate new map
            self.gameMap.new();
            self.currentRoom = self.gameMap.startRoom()

            # Move player
            self.player.rect.left = 1 * size
            self.player.rect.top = 11 * size
            self.player.inPortal = False

        return self.player.life > 0

    def draw(self, screen):
        """
        Draw game objects

        returns dirty rectangles
        """

        # Draw room
        dirty = self.currentRoom.draw(screen, self.camera)

        # Draw player
        dirty += self.player.draw(screen, self.camera)

        # Draw hazards
        for hazard in self.currentRoom.hazards:
            screen.blit(hazard.image, self.camera.apply(hazard))

        # Draw UI
        screen.blit(self.ui.image, (0, 0))
        dirty.append(Rect(0, 0, DISPLAY[0], 120))

        # Draw minimap
        screen.blit(self.gameMap.miniMap(self.currentRoom), (DISPLAY[0]-8*8*2-120, 0))

        # Handle dirty rectangles
        if self.camera.dirty:
            dirty += self.camera.oldDirty + self.camera.newDirty

        return dirty
