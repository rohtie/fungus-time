import pygame
from pygame import *
from settings import *
from PIL import Image
from PIL import ImageOps

sizeMultiplier = 2


def loadSprite(name, convert=True, colorkey=Color("#000000")):
    """
    Load a sprite and scale it to twice size
    """

    if 'assets' in name:
        image = pygame.image.load(name + ".png")
    else:
        image = pygame.image.load("assets/sprites/" + name + ".png")

    if convert:
        image = image.convert()

    if colorkey:
        image.set_colorkey(colorkey)

    w, h = image.get_size()
    return pygame.transform.scale(image, (w*sizeMultiplier, h*sizeMultiplier))


def onScreen(rectangle):
    """
    Checks if a rectangle is within screen coordinates.
    """

    x, y = rectangle.topleft
    w, h = rectangle.size

    if (-w < x < sw) and (120-h < y < sh):
        return True

    return False


def playMusic(name):
    """
    Load music and loop indefinetely
    """

    if music:
        pygame.mixer.music.load("assets/music/" + name + ".ogg")
        pygame.mixer.music.play(-1)


def createDamageFeedback(image):
    # Convert surface to PIL image
    pil_string_image = pygame.image.tostring(image, "RGB", False)
    pil_image = Image.frombytes("RGB", image.get_size(), pil_string_image)

    # Colorize the image to a red hue
    pil_image.load()
    gray = ImageOps.grayscale(pil_image)
    gray = ImageOps.autocontrast(gray)
    tmp_image = ImageOps.colorize(gray, "#000000", "#ff0000")

    # Convert to pygame surface
    mode = tmp_image.mode
    size = tmp_image.size
    data = tmp_image.tobytes()

    image = pygame.image.fromstring(data, size, mode)
    image.set_colorkey(Color("#000000"))

    return image
