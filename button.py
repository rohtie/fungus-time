import pygame
from pygame import *


class Button(pygame.sprite.Sprite):
    """
    Button class for use in main menu
    """

    def __init__(self, text, pos):
        pygame.sprite.Sprite.__init__(self)

        # Load button sprite
        self.normal = pygame.image.load("assets/bg/" + text + ".png").convert_alpha()
        self.hover = pygame.image.load("assets/bg/" + text + "_h.png").convert_alpha()
        self.image = self.normal
        self.rect = self.image.get_rect(topleft=pos)

    def mouseover(self, mpos):
        """
        Check if mouse cursor hover over self and apply hover color
        """

        if self.rect.collidepoint(mpos):
            self.image = self.hover
            return True

        else:
            self.image = self.normal
            return False


