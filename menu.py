from button import *
from settings import *
from macro import *


class Menu:
    """
    Main menu
    """

    def __init__(self):

        # Title graphics
        self.bg = pygame.image.load("assets/bg/fungustime.png")

        # Buttons
        self.play_b = Button("play", (120, sh-80))
        self.fullscreen_b = Button("fullscreen", (sw-320, sh-80))

        self.buttons = [self.play_b, self.fullscreen_b]

        self.fullscreen = False

    def handleEvent(self, e):
        """
        Handle events on main menu

        returns true on game start
        """

        # Check for click
        if e.type == MOUSEBUTTONUP:
            if self.play_b.mouseover(e.pos):
                # Start the game
                return True


            elif self.fullscreen_b.mouseover(e.pos):
                # Toggle fullscreen
                if self.fullscreen:
                    display.set_mode(DISPLAY)
                else:
                    display.set_mode(DISPLAY, FULLSCREEN)

                self.fullscreen = not self.fullscreen

        # Update hover
        if e.type == MOUSEMOTION:
            for button in self.buttons:
                button.mouseover(e.pos)

        return False

    def draw(self, screen, playerAlive):
        """
        Draw the main menu
        """

        screen.blit(self.bg, (0, 0))

        for button in self.buttons:
            screen.blit(button.image, button.rect)
