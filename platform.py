import pygame
from pygame import *
from settings import *
from macro import *


class Platform(pygame.sprite.Sprite):
    """
    General purpose platform used to calculate collisions.
    """

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)

        # Debug sprite for platform
        self.image = Surface((size, size))
        self.image.convert()
        self.image.fill(Color("#8F2F2F"))
        self.rect = Rect(x, y, size, size)


class Step(Platform):
    """
    Platform that can be passed through by double tapping
    """

    def __init__(self, x, y):
        Platform.__init__(self, x, y)

        # Debug sprite for step
        self.image = Surface((size, 3))
        self.image.convert()
        self.image.fill(Color("#7F1F1F"))
        self.rect = Rect(x, y, size, 3)


class AIwall(Platform):
    """
    Invisible wall for enemies
    """

    def __init__(self, x, y):
        Platform.__init__(self, x, y)

        self.rect = Rect(x, y, size, size/2)
        self.rect.top += size/2

