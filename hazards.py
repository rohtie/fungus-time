import pygame
from pygame import *
from settings import *
from macro import *


class Hazard(pygame.sprite.Sprite):
    """
    Hazard super class
    """

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)

        # Debug sprite for hazard
        self.rect = Rect(x, y, size, size)


class Spike(Hazard):
    """
    Spikes, subclass of Hazard
    """

    def __init__(self, x, y):
        Hazard.__init__(self, x, y)

        # Debug sprite for spikes/thorns
        self.image = loadSprite("thorn_middle")
        self.image.set_colorkey(Color("#000000"))

        self.rect = Rect(x, y, size, size)
