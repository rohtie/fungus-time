import pygame
from pygame import *
from settings import *
from macro import *
from message import *


class Item(pygame.sprite.Sprite):
    """
    Super class for items
    """

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.rect = Rect(0, 0, size, size)
        self.hold = False

        self.name = 'Unspecified'

        # Default powerup values
        self.brange = 0
        self.damage = 0
        self.rate = 0
        self.lives = 0
        self.life = 0
        self.speed = 0

        self.count = 100

    def apply(self, player):
        """
        Apply item stats to player
        """

        # Apply stats
        if not self.hold:
            if player.range < 500:
                player.range += self.brange

            if player.damage < 6:
                player.damage += self.damage

            if player.rate > 5:
                player.rate -= self.rate

            if player.lives < 16:
                player.lives += self.lives

            if player.life < player.lives:
                player.life += self.life

            if player.speed < 5:
                player.speed += self.speed

            self.hold = True

    def stillHolding(self, room):
        """
        Check if player is holding item
        """

        # No longer holding, destroy
        if self.count <= 0:
            room.items.remove(self)
            return False

        return True

    def update(self, player):
        """
        Float item above player
        """

        if self.hold:
            self.count -= 1
            self.rect.bottom = player.rect.top - 8
            self.rect.left = player.rect.left - 4

    def getMessage(self):
        return Message(500, 32, self.name)


class ItemSpawner:
    """
    Class for spawning items
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y



class Muscaria(Item):
    """
    All stats up
    """

    def __init__(self, x, y):
        Item.__init__(self)

        self.name = 'Amanita muscaria'

        # Set sprite
        self.image = loadSprite("muscaria")
        self.rect.topleft = (x, y)

        # Stat upgrade
        self.lives = 1
        self.life = 1
        self.damage = 1
        self.rate = 5
        self.brange = 100


class Luridiformis(Item):
    """
    +hp
    -speed
    +rate
    """

    def __init__(self, x, y):
        Item.__init__(self)

        self.name = 'Boletus luridiformis'

        # Set sprite
        self.image = loadSprite("luridiformis")
        self.rect.topleft = (x, y)

        # Stat upgrade
        self.lives = 1
        self.life = 1
        self.rate = 5
        self.damage = 1


class Cibarius(Item):
    """
    hp + range
    """

    def __init__(self, x, y):
        Item.__init__(self)

        self.name = 'Cantharellus cibarius'

        # Set sprite
        self.image = loadSprite("cibarius")
        self.rect.topleft = (x, y)

        # Stat upgrade
        self.lives = 1
        self.life = 1
        self.brange = 100
        self.damage = 1


class Punicea(Item):
    """
    hp + damage
    """

    def __init__(self, x, y):
        Item.__init__(self)

        self.name = 'Hygrocybe punicea'

        # Set sprite
        self.image = loadSprite("punicea")
        self.rect.topleft = (x, y)

        # Stat upgrade
        self.lives = 1
        self.life = 1
        self.damage = 1


class Atromentarius(Item):
    """
     -speed +rate
    """

    def __init__(self, x, y):
        Item.__init__(self)

        self.name = 'Coprinus atromentarius'

        # Set sprite
        self.image = loadSprite("atromentarius2")
        self.rect.topleft = (x, y)

        # Stat upgrade
        self.speed = -0.5
        self.rate = 5
        self.damage = 1



