Fungus Time is a game about exploring and hunting mushrooms in the autumn forest, filled by nasty snails hungry for fungi flesh and deliciously beautiful mushrooms.

Charlie loves two things in life: fungi and playing the trombone. On the way to his concert, he is diverted from his path by his favorite mushroom spot. However, only traces of fungi are found!

Future plans
============

I have some ideas for where I want to go with this game, mostly small enchantments and more content, but changes anyhow.

Immersion:
----------

  Rain, falling leaves and other environmental sugar.

  I am also playing with the thought about a day/night cycle, though I am not sure if it would work with the current graphic style (might be a tad too dark).

  I had an idea about adding different seasons, so that as the game progresses it's starting to snow and the environment is changing. However this unfortunately crashes with the main theme of Fungus Time which are fungi and autumn.

Gameplay:
---------

  I want to add more variety of items to make the gameplay more interesting. For example more attacks. Salt could be a nice weapon against snails.

  More mushrooms with more variety of effects. Effects that apply to the trombone notes. Changing their pattern, color, adding area effects and so on.

  More variety of levels, enemies and bosses.

Story:
------

  At the end after winning over the mushroom thief (boss slug); Charlie continues on his path to the concert, which is to be played at his home. Charlie cooks the fungi he has gathered and plays trombone for his guests.

Code style:
-----------

  I started programming this for a python course I took at the university in 2013. Since then I have worked on other unrelated projects for work and such where I have learned more about the pythonic ways of doing thing. So the first thing that needs to be done is to style all the code according to PEP8 standards. Then I have to deal with the amount of duplicate code that is everywhere in a huge refactor.

Installing dependencies
=======================

This game is dependent on the following:

    python2.7
    pygame
    Python Imaging Library (PIL or Pillow)

## Linux
These packages should be easily installable from your favorite linux ditributions' repositories.
python2.7 is already installed in most linux distributions.

**Debian** (probably works for ubuntu and other debian-deratives):
    `sudo apt-get install python-pygame python-pil`

**Arch linux**:
    `sudo pacman -S python2-pygame python2-pillow`

## Windows

Download and install these:
[https://www.python.org/ftp/python/2.7.6/python-2.7.6.msi](https://www.python.org/ftp/python/2.7.6/python-2.7.6.msi)
[http://effbot.org/downloads/PIL-1.1.7.win32-py2.7.exe](http://effbot.org/downloads/PIL-1.1.7.win32-py2.7.exe)
[http://pygame.org/ftp/pygame-1.9.1.win32-py2.7.msi](http://pygame.org/ftp/pygame-1.9.1.win32-py2.7.msi)

Run game
========
    Run main.py using python2.7 with the dependencies above installed.

Controls
========
    Arrows keys : Move character
              z : Jump
              x : Play trombone
    down + jump : Pass through steps

Showcase keys
=============
    b : Teleport to boss room
    t : Teleport to treasure room
    p : Clear room of enemies

Leveleditor
===========
    Change directory to leveleditor and run 'leveledit.py'

      leftclick : Place a tile
     rightclick : Remove tile
    alt + click : Pick a tile/platform from map

              z : Switch between tile and platform mode
              x : Toggle platforms
              c : Toggle tiles
              v : Toggle transparent platforms
           left : Previous room
          right : Next Room

              n : New Room
              s : Save current room

Credits
=======

Thor Merlin Lervik
----------
Pixel art, game design, concept, story and programming

Aslak Hauglid
----------
Narration

DST - http://www.nosoapradio.us/
----------
Music

Special thanks:
----------
  Katarina Caspersen

  Jardar Følling

