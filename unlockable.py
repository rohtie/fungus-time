import pygame
from pygame import *
import random
from consumable import *
from item import *
from macro import *


class Unlockable(pygame.sprite.Sprite):
    """
    super class for unlockable objects
    """

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)


class Chest(Unlockable):
    """
    Chest that can be unlocked without a key, subclass of Unlockable
    """

    def __init__(self, x, y):
        Unlockable.__init__(self)

        # Set default chest sprite
        self.rect = Rect(x, y, 48*sizeMultiplier, 48*sizeMultiplier)

        self.image = loadSprite("chest")
        self.image.set_colorkey(Color("#000000"))

        # Position chest
        self.rect.bottom -= 16*sizeMultiplier
        self.rect.left -= 8*sizeMultiplier

        self.locked = True

    def unlock(self, items, room):
        """
        Unlock the chest if possible and spawn consumables or an item.
        """

        # Change sprite
        self.image = loadSprite("chest_open")
        self.image.set_colorkey(Color("#000000"))
        self.locked = False

        item = 0

        # Decide how many consumables to spawn
        if isinstance(self, LockedChest):
            numConsumables = random.randint(1, 7)
            item = random.randint(0, 1) #50-50 chance of getting item

        else:
            numConsumables = random.randint(1, 3)

        # Spawn consumables from chest
        if item == 0 or not items:
            for i in range(0, numConsumables):
                ctype = random.randint(0, 10)

                x, y = self.rect.center

                # Key
                if ctype < 2:
                    room.addConsumable (Key, x, y)

                # Heart
                elif ctype >= 2 and ctype <= 4:
                    room.addConsumable (Heart, x, y)

                # Money
                else:
                    room.addConsumable (Money, x, y)

        # Spawn random item
        else:
            if items:
                item = items.pop()
                room.items.append(item(self.rect.x + 8, self.rect.y - 8))


class LockedChest(Chest):
    """
    Chest that can be unlocked with a key, subclass of Chest
    """

    def __init__(self, x, y):
        Chest.__init__(self, x, y)

        # Change sprite
        self.image = loadSprite("chest_locked")
        self.image.set_colorkey(Color("#000000"))