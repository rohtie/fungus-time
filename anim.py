import pygame
from pygame import *
import glob
from macro import *


class Animation(pygame.sprite.Sprite):
    """
    Class used to emulate animation by displaying one frame at a time
    """

    def __init__(self, id, colorkey, loop=True, speed=5, damage_filter=lambda x: x):

        # Load images
        self.left = [ loadSprite(frame[:-4]) for frame in sorted(glob.glob("./assets/sprites/" + id + "*.png")) ]

        # Set colorkey
        for frame in self.left:
            frame.set_colorkey(Color(colorkey))

        # Make a flipped version
        self.right = [ pygame.transform.flip(frame, True, False) for frame in self.left ]

        if damage_filter:
            self.damagedLeft = [ damage_filter(frame.copy()) for frame in self.left ]
            self.damagedRight = [ pygame.transform.flip(frame, True, False) for frame in self.damagedLeft ]

        self.frames = self.left

        self.frame = 0
        self.counter = 0

        self.loop = loop
        self.done = False
        self.speed = speed

    def next(self, right, damaged=False):
        """
        Return next frame
        """

        if self.loop or self.frame < len(self.frames) -1:
            self.advance()
        else:
            self.done = True

        return self.current(right, damaged)

    def advance(self):
        """
        Advance frame count without returning
        """

        if self.counter % self.speed == 0:
            self.frame += 1

            if self.frame > len(self.frames) -1:
                self.frame = 0
                self.counter = 0

        self.counter += 1

    def current(self, right, damaged=False):
        """
        Return current frame
        """

        if damaged:
            if right:
                self.frames = self.damagedRight
            else:
                self.frames = self.damagedLeft
        else:
            if right:
                self.frames = self.right
            else:
                self.frames = self.left

        return self.frames[self.frame]

    def reset(self):
        """
        Reset animation to first frame
        """

        self.frame = 0
        self.counter = 0
