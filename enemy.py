import pygame
from pygame import *
from anim import *
import math
import random
import bullet
from shadow import *
from macro import *

from math import radians as deg2rad


class Enemy(pygame.sprite.Sprite):
    """
    General purpose enemy class
    """

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)

        # Enemy stats
        self.life = 0
        self.speed = 0

        # Enemy variables
        self.right = True
        self.yvel = 0
        self.xvel = 0
        self.hitWall = False
        self.hitCeiling = False
        self.died = False
        self.hitDuration = 10 # Set to 10 to prevent enemy blinking on entering a new room

        # Enemy graphics
        self.anim = None
        self.damagedAnim = None
        self.deathAnim = None
        self.image = None
        self.rect = None
        self.shadow = None

    def think(self, player, room):
        """
        AI part of enemy
        """

        pass

    def update(self, solidObjects):
        """
        Update enemy animation and check for collisions.
        """

        # Get next frame of animation
        if self.hitDuration < 6:
            self.hitDuration += 1
            self.image = self.anim.next(self.right, True)
        else:
            self.image = self.anim.next(self.right)

        # Check collisions
        self.checkCollisions(solidObjects)

        # Calculate next shadow position
        self.shadow.update(self, solidObjects)

    def checkCollisions(self, solidObjects):
        """
        Apply xvel/yvel and check collisions to solid objects
        """

        self.rect.left += self.xvel

        # Check collision with platforms on x-axis
        for solid in solidObjects:
            if sprite.collide_rect(self, solid):

                if self.xvel > 0:
                    self.xvel = 0
                    self.rect.right = solid.rect.left - 1

                    self.hitWall = True

                if self.xvel < 0:
                    self.xvel = 0
                    self.rect.left = solid.rect.right + 1

                    self.hitWall = True

        self.rect.top += self.yvel

        # Check collision with platforms on y-axis
        for solid in solidObjects:
            self.onGround = False

            if sprite.collide_rect(self, solid):
                if self.yvel > 0:
                    self.rect.bottom = solid.rect.top
                    self.onGround = True
                    self.yvel = 0

                if self.yvel < 0:
                    self.rect.top = solid.rect.bottom
                    self.yvel = self.yvel * -1

                    self.hitCeiling = True

    def hit(self):
        self.hitDuration = 0

    def isDead(self):
        """
        Check if enemy has any life left
        """

        if self.life <= 0:
            if self.deathAnim:
                self.anim = self.deathAnim

                if not self.died and self.rect.h < self.deathAnim.current(self.right).get_height():
                    self.rect.top -= self.deathAnim.current(self.right).get_height() - self.rect.h

            self.died = True

            return True

        return False



class FlyingEnemy(Enemy):
    """
    Super class for flying enemies, subclass of Enemy
    """

    def __init__(self):
        Enemy.__init__(self)


class GroundEnemy(Enemy):
    """
    Super class for ground enemies, subclass of Enemy
    """

    def __init__(self):
        Enemy.__init__(self)

        self.onGround = False

    def update(self, solidObjects):
        """
        Apply physics and check collisions
        """

        # Get next frame of animation
        if self.hitDuration < 6:
            self.hitDuration += 1
            self.image = self.anim.next(self.right, True)
        else:
            self.image = self.anim.next(self.right)

        # Apply gravity
        if not self.onGround:
            self.yvel += 0.3*sizeMultiplier

            if self.yvel > 30*sizeMultiplier:
                self.yvel = 30*sizeMultiplier

        # Check collisions
        self.checkCollisions(solidObjects)

        self.shadow.rect.topleft = self.rect.bottomleft
        self.shadow.rect.y -= 8


class Mosquito(FlyingEnemy):
    """
    Mosquito enemy, subclass of FlyingEnemy
    """

    def __init__(self, x, y):
        FlyingEnemy.__init__(self)

        # Init values
        self.life = 3
        self.speed = 0.5*sizeMultiplier

        # Set default sprite
        self.deathAnim = Animation("death2_flying_slug", "#000000", False)
        self.anim = Animation("fly_slug", "#000000", True, 1, damage_filter=createDamageFeedback)
        self.image = self.anim.next(self.right)
        self.rect = self.image.get_rect(topleft=(x, y))
        self.shadow = Shadow("shadow_slug")

    def think(self, player, room):
        """
        Mosquito AI

        Attacks player if within reach else flies around at random
        """

        w, h = self.image.get_size()

        # Make mosquito follow player if within range
        if (-200*sizeMultiplier < (self.rect.x + w/2 - player.rect.x) < 200*sizeMultiplier) and (-100*sizeMultiplier < (self.rect.y + h/2 - player.rect.y) < 100*sizeMultiplier):
            if self.rect.x > player.rect.x:
                self.right = False
                self.xvel -= self.speed
            else:
                self.right = True
                self.xvel += self.speed

            if self.rect.y > player.rect.y:
                self.yvel -= self.speed/2
            else:
                self.yvel += self.speed/2

        # Random idling
        else:
            action = random.randint(0, 5)
            direction = random.randint(0, 3)

            if action == 0:
                if direction == 0:
                    self.xvel -= self.speed*2
                    self.right = False

                elif direction == 1:
                    self.xvel += self.speed*2
                    self.right = True

                elif direction == 2:
                    self.yvel -= self.speed
                else:
                    self.yvel += self.speed

        # Enforce max speed
        if self.xvel > 3:
            self.xvel = 3

        elif self.xvel < -3:
            self.xvel = -3

        if self.yvel > 3:
            self.yvel = 3

        elif self.yvel < -3:
            self.yvel = -3


class Slug(GroundEnemy):
    """
    Slug enemy, subclass of GroundEnemy
    """

    def __init__(self, x, y):
        GroundEnemy.__init__(self)

        # Init values
        self.life = 3
        self.speed = 2*sizeMultiplier

        # Set default sprite
        self.deathAnim = Animation("death_slug", "#000000", False)
        self.anim = Animation("slug", "#000000", damage_filter=createDamageFeedback)
        self.image = self.anim.next(self.right)
        self.rect = self.image.get_rect(topleft=(x, y))
        self.shadow = Shadow("shadow_slug")

    def think(self, player, room):
        """
        Slug AI

        Change direction when hitting a wall
        """

        if self.hitWall:
            self.right = not self.right
            self.hitWall = False

        if self.right:
            self.xvel = self.speed
        else:
            self.xvel = -self.speed

# Needs major rework!
class Hopper(GroundEnemy):
    """
    Hopper enemy, subclass of GroundEnemy
    """

    def __init__(self, x, y):
        GroundEnemy.__init__(self)

        # Init values
        self.life = 3
        self.speed = 2*sizeMultiplier

        # Set default sprite
        self.anim = Animation("slug", "#000000")
        self.image = self.anim.next(self.right)
        self.rect = self.image.get_rect(topleft=(x, y))
        self.shadow = Shadow("shadow_slug")

    def think(self, player, room):
        """
        Hopper AI

        Has a chance of jumping forward
        """

        rnd = random.randint(0, 500)

        if 0 < rnd < 3:
            self.yvel = -10

        if self.hitWall or rnd == 0:
            self.right = not self.right
            self.hitWall = False

        if self.right:
            self.xvel = self.speed
        else:
            self.xvel = -self.speed


class SlugBoss(GroundEnemy):
    """
    Slug boss, subclass of GroundEnemy
    """

    def __init__(self, x, y):
        GroundEnemy.__init__(self)

        # Init
        self.speed = 5*sizeMultiplier
        self.life = 40
        self.attack = 0
        self.wait = 0

        # Load animations
        self.deathAnim = Animation("boss_death", "#000000", False)
        self.attack_anim = Animation("boss_attack", "#000000", damage_filter=createDamageFeedback)
        self.walk_anim = Animation("boss_walk", "#000000", damage_filter=createDamageFeedback)

        # Set default animation
        self.anim = self.walk_anim
        self.image = self.anim.next(self.right)

        # Setup mask
        self.leftMask = pygame.mask.from_surface(loadSprite("boss_mask"))
        self.rightMask = pygame.mask.from_surface(pygame.transform.flip(loadSprite("boss_mask"), True, False))
        self.mask = self.leftMask

        self.rect = Rect((x, y), self.image.get_size())

        # Shadow
        self.shadow = Shadow("shadow_boss")

    def think(self, player, room):
        """
        Boss AI
        """

        if self.right:
            self.mask = self.rightMask
        else:
            self.mask = self.leftMask

        if self.life < 20:
            self.speed = 10
        elif self.life < 5:
            self.speed = 15

        if self.hitWall:
            self.right = not self.right
            self.hitWall = False

            # Random chance of attacking
            if self.life < 40:
                if random.randint(0, 1) == 0:
                    self.attack = 30

        # Attack
        if self.attack > 0:
            if self.attack == 25:
                attackType = random.randint(0, 1)

                # Find correct side of boss
                if self.right:
                    x, y = self.rect.topright
                else:
                    x, y = self.rect.topleft

                # Shoot slime
                for i in range(random.randint(1, 3)):
                    if self.right:
                        room.bullets.append(bullet.SlugSlime(x, y, deg2rad(random.randint(280, 350)), player, random.randint(5, 7)))
                    else:
                        room.bullets.append(bullet.SlugSlime(x, y, deg2rad(random.randint(190, 260)), player, random.randint(5, 7)))

            self.anim = self.attack_anim
            self.attack -= 1

        # Walk to the other end
        else:
            self.anim = self.walk_anim

            if self.right:
                self.xvel = self.speed
            else:
                self.xvel = -self.speed

