.. Fungus Time documentation master file, created by
   sphinx-quickstart on Thu Oct 10 16:37:38 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Fungus Time
***********
.. toctree::
   :maxdepth: 2

   rapport
   codedoc

**Fungus Time**

Fungus Time er et roguelike platform-spill hvor spilleren utforsker en tilfeldig generert verden.

.. Du blir satt i rollen til Charlie McDowell, en informatikk-professor som spiller trombone på fritiden. På vei for å spille på en blåsekonsert tar han en snarvei gjennom skogen. Snarveien leder forbi hans favoritt sopp-sted. Siden all soppen er borte følger Charlie sporet som leder dypere inn i skogen for å finne tyven.

Last ned Spillet
================

Last ned spillet her: :download:`Fungus Time <download/thorml_fungustime.tar.gz>`

Gameplay
========

.. raw:: html

      <!-- This version of the embed code is no longer supported. Learn more: https://vimeo.com/help/faq/embedding --> <object width="726" height="512"><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=77875909&amp;force_embed=1&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=1&amp;color=00adef&amp;fullscreen=1&amp;autoplay=0&amp;loop=0" /><embed src="http://vimeo.com/moogaloop.swf?clip_id=77875909&amp;force_embed=1&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=1&amp;color=00adef&amp;fullscreen=1&amp;autoplay=0&amp;loop=0" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="726" height="512"></embed></object>