.. heliarma documentation master file, created by
   sphinx-quickstart on Thu Oct 10 16:37:38 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Prosjektrapport
***************

Motivasjon
==========

**Utvikle et utvidbart basis-spill**

Jeg har tidligere lagd et par spill både på fritiden og i forbindelse med game jams. Det mine tidligere spill har til felles er at de ikke er så veldig avanserte. De bærer preg av hurtig utvikling, og skitne løsninger.

**Utvikle et større spillprosjekt**

De største prosjektene jeg har gjennomført tidligere har vært innenfor webutvikling, jeg har blant annet skrevet forum programvare og tre forskjellige CMS(Content Management System). De fleste spillene jeg har laget før har vært veldig små i forhold.

**Lage et spill**

Utvikling av spill er veldig gøy og lærerikt. Det er mange problemstillinger i spillutvikling man ikke støter på andre steder. I motsetning til mange andre typer utvikling så ser en fremgang nesten med en gang. Det er lettere å oppdage bugs siden en jobber med et visuellt system. Sluttproduktet øker så motivasjonen til å gå for enda mer ambiøse ideer ved neste prosjekt.

**Skrive godt dokumentert kode**

Dette er noe man aldri blir for god til.

Funksjonsmål
============

Dette er funksjonaliteten jeg orginalt ville implementere::

    Spillerstyring
    2d platform-fysikk
    Forskjellige typer blokker (platformer, pigger, ol)
    Rom-design hentes fra bildefiler
    Side-scrolling
    UI til å vise antall livspoeng og andre ting
    Våpen
    Items som gjør spilleren sterkere/raskere/etc
    Skattekister
    Forskjellige typer dører (Låste, bossrom, etc)
    Legge til animerte sprites
    Kunstig intelligens til fiender og boss
    Meny GUI
    Legge til lyder/musikk

Funksjonalitet jeg implementerte i tilegg::

    Leveleditor
    Skygger

Funksjonalitet jeg har lyst til å implementere senere::

    Intro med narration (story)
    Credits
    Bakgrunnsgrafikk (slik at det ser ut som spilleren faktisk er i en skog)
    Dynamisk lys

Grafikk og Musikk
=================

I dette prosjektet har jeg tegnet og animert alle grafiske elementer med `aseprite <http://www.aseprite.org/ />`_.

Musikken har jeg lastet ned fra http://www.nosoapradio.us/ som tilbyr gratis spill-musikk lisensiert under `Creative Commons Attribution License <http://creativecommons.org/licenses/by/3.0/ />`_.

Evaluering
==========

Jeg har jobbet veldig gjevnt utover hele prosjektet og hatt det veldig gøy. All planlagt funksjonalitet er implementert pluss litt mer. Sluttproduktet er så og si slik jeg hadde forestilt meg. Koden er skrevet på en måte som kan lett bygges videre på. Dette skyldes objektorientering og veldokumentert kode.

Grafisk sett kom jeg ikke helt i mål. De flygende sneglene skulle egentlig vært mygg. Det bossen skyter skal egentlig være slimballer. Det skulle egentlig ha vært grafisk bakgrunn på hovedmenyen.

Erfaringer
==========

**Lagre romdata i bildefiler**

Dette var en veldig god ide, ihvertfall i begynnelsen. Ved å gjøre det slik kunne jeg raskt og enkelt lage test-rom uten å ha en leveleditor. Trenger bare et tegneprogram. Problemene begynte å komme når jeg la til tiles, da ble tiledata litt vel komplekst å tegne for hånd (masse indekser å holde styr på). Derfor fant jeg ut at det var lurt å lage en leveleditor i tilegg, slik fikk jeg større oversikt over hvordan rommene kom til å se ut. På mitt neste spill-prosjekt kommer leveleditor til å være blant en av de første tingene jeg implementerer.

**Framerate**

Jeg har hatt problemer med framerate og derfor brukt litt vel mye tid på å fikse det. Jeg sjekket først med python sitt profiling verktøy, uten å få noe særlig ut av det. Så jeg gjorde litt research og fant ut at dirty rectangle animation kan være en måte å gjøre frameraten bedre. Slik løste jeg nesten alle framerate-problemer. Mens jeg testet litt forskjellige display modes fant jeg ut at grunnen til framerate-problemene egentlig stammet fra at jeg hadde satt color depth til 16 bit. Nå lar jeg pygame selv bestemme hvilken depth som egner seg best. Etter å ha gjort dette, kjørte faktisk spillet raskere uten dirty rectangles.

Selv om jeg brukte mye tid på å fikse framerate, og løsningen egentlig var veldig enkel. Har det ikke vært bortkastet. Jeg har fått større innsikt i hva som kan gjøre spill treige, og hvordan en kan løse slike problemer. Det har også gitt meg innsikt i andre teknologier, slik som hvordan video kan komprimeres.

**Sphinx**

Jeg brukte sphinx for å generere denne websiden. Noe jeg angrer litt på, siden man med reStructuredText ikke har like mye frihet som med html. Jeg føler at websiden ikke ble like oversiktlig som jeg hadde tenkt. Dersom jeg hadde lagd denne websiden med php, html og css; hadde den nok sett bedre ut og hatt et mer intuitivt navigasjons-system. På en annen side hadde det nok tatt mye lengre tid. Så på en "time vs effort" måte, var nok dette den beste løsningen.

Timeliste
=========

Jeg har bare notert tid brukt på programmeringen, siden grafikk ikke er relevant for INF3331.

::

    aug 22 -   7 timer - Platformer og spiller-fysikk
    aug 25 -   5 timer - Scrolling, pigger, nøkler og hjerter
    aug 26 -   2 timer - UI for hjerter og enkel fiende
    aug 29 -   2 timer - Skattekister og romdata leses inn fra bildefil
    aug 30 - 1.5 timer - Tilfeldig kart
    sep  2 - 1.5 timer - Hele kartet kan nå besøkes
    sep  3 - 0.5 time  - Lukket åpninger som ledet til ingensteder
    sep  5 -   4 timer - Nøkler og hjerter har fysikk og flyr ut av skattekiste.
    sep  9 -   9 timer - Skudd, spiller-dukking
    sep 10 -   3 timer - Penger og genererte noter
    sep 12 -   8 timer - Oppgraderinger, spiller-animasjoner og boss
    sep 14 -   3 timer - La inn mer grafikk
    sep 16 -   1 time  - La inn dører som åpner seg når rommet er cleared
    sep 19 -   8 timer - Leveleditor
    sep 20 - 3.5 timer - Fikset noen leveleditor bugs og la inn tiles til spillet
    sep 27 -   1 time  - La inn masse sprites og animasjoner
    oct  2 - 0.5 time  - Fikset tiles som er rundt dørene
    oct  3 - 0.5 time  - La inn musikk og fall damage
    oct  4 - 1.5 timer - La inn skygger
    oct  5 -   8 timer - Gjorde bossen vanskeligere og la inn masse sprites
    oct  6 - 1.5 timer - La inn dør-sprites og implementerte hovedmeny
    oct  7 -   8 timer - Dirty rectangles for tiles
    oct  9 -   1 time  - Dirty rectangles for player
    oct 10 -   4 timer - Nesten ferdig implementert dirty rectangles, framerate fikset med display mode

Ut fra timelisten kan en se at jeg har brukt ca. 61 timer på å implementere planlagt funksjonalitet.
10 timer på å implementere ekstra funksjonalitet, og 13 timer på å fikse framerate.

Til sammen blir det 84 timer.
