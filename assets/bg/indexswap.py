from PIL import Image
import glob

# swap = {
# 21 : 67,
# 19 : 65,
# 18 : 64
# }
swap = {
    0 : 13,
    # 18 : 186,
    # 19 : 114,
    # 21 : 66,
}

name = str(raw_input())

# self.frames = sorted(glob.glob("./assets/bg/" + name + "*.png"))

# for frame in frames:

# Load image
im = Image.open(name)
data = im.load()

# Parse
w, h = im.size
for y in range(0, h):
    for x in range(0, w):
        if data[x, y] in swap:
            data[x, y] = swap[data[x, y]]

im.save(name)