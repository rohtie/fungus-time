import pygame
from pygame import *


class Credits:
    """
    Handle the credits show
    """

    def __init__(self):
        self.image = pygame.image.load("assets/bg/credits.png")
        self.y = 0
        self.h = self.image.get_height()

        self.alpha = 256
        self.image.set_alpha(self.alpha - 1)

    def draw(self, screen):
        if self.alpha == 0:
            return False

        if -self.y > self.h - 576:
            self.alpha -= 32
            self.image.set_alpha(self.alpha - 1)


        screen.blit(self.image, (0, self.y))
        self.y -= 1

        return True
