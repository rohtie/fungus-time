import pygame
from pygame import *
from introslide import *


class Intro:
    """
    Handle intro slides, draw intro and play narration
    """

    def __init__(self):

        # Init slides
        self.currentSlide = 0

        self.slides = [
            IntroSlide("slide1", 12, True, 8, True, (1, 0), Color("#ff55ff")),
            IntroSlide("slide2", 12, True, 9, True, (0.1, 0.1)),
            IntroSlide("blank", 30, False, 9, True, (-0.1, 0)),
            IntroSlide("slide4", 30, False, 5),
            IntroSlide("filler", 30, False, 0.5),
            IntroSlide("filler", 30, False, 5)
        ]

        self.finished = False
        self.title = pygame.image.load("assets/bg/fungustime.png")
        self.title.set_colorkey(Color("#000000"))
        self.title.set_alpha(0)
        self.titleTextAlpha = 0

        self.black = Surface(self.title.get_size())
        self.black = self.black.convert()
        self.black.fill((0, 0, 0))
        self.black.set_alpha(0)

        self.fade = False
        self.fadeIn = False
        self.fadeOut = False
        self.fadeAlpha = 0

    def draw(self, screen):
        current = self.slides[self.currentSlide]

        # Start slide
        if not current.started:
            current.start()

        # Draw slide
        screen.blit(current.next(), (0, 0))

        # Draw title fading in at last slide
        if self.currentSlide == len(self.slides) - 1:
            screen.blit(self.title, (0, 0))
            self.titleTextAlpha += 1
            self.title.set_alpha(int(self.titleTextAlpha))

        # Check if finished
        if current.finished:
            if self.currentSlide == len(self.slides) - 1:
                self.finished = True

            else:
                self.fadeOut = True
                self.currentSlide += 1

        # Fade between slides
        if self.fade:
            if current.fade:
                self.fadeIn = True
                current.fade = False

            if self.fadeIn:
                if self.fadeAlpha == 100:
                    self.fadeIn = False

                self.fadeAlpha += 10
                self.black.set_alpha(int(self.fadeAlpha))
                screen.blit(self.black, (0, 0))

            if self.fadeOut:
                if self.fadeAlpha == 0:
                    self.fadeOut = False

                self.fadeAlpha -= 10
                self.black.set_alpha(int(self.fadeAlpha))
                screen.blit(self.black, (0, 0))
