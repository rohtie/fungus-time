import pygame
from pygame import *
from macro import *
from settings import *


class Tile(pygame.sprite.Sprite):
    """
    General purpose tile class.
    """

    def __init__(self, tilesheet, tx, ty, x, y):
        pygame.sprite.Sprite.__init__(self)

        # Set to correct tile
        self.image = Surface((size, size))
        self.image.blit(tilesheet, (-tx, -ty))
        self.image.set_colorkey(Color("#071210"))

        self.rect = Rect(x, y, size, size)