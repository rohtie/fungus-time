import pygame
from pygame import *
from settings import *
from macro import *


class Decal(pygame.sprite.Sprite):
    """
    Super class for decals

    cords:    Coordinates
    name:    Name of sprite
    """

    def __init__(self, cords, name):
        pygame.sprite.Sprite.__init__(self)

        # Set sprite
        self.image = loadSprite(name)

        w, h = self.image.get_size()
        self.rect = Rect(0, 0, w, h)
        self.rect.midbottom = cords
