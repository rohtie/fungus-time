from pygame import *
import pygame
import os


class Message:
    """
    Generate message image for display.
    """

    def __init__(self, w, h, message):

        # Blank out new image
        image = Surface((w, h))
        image.fill((0, 0, 0))

        # Load font
        self.font = pygame.font.Font(os.path.abspath("assets/fonts/8-BIT-WONDER-by-Joiro-Hatagaya.ttf"), 20)

        # Draw message
        image.blit(self.font.render(message, 0, Color("#FFFFFF")), (10, 2))

        self.image = image
