import math
import pygame
from pygame import *
from platform import *
from enemy import *
from anim import *
import random
from settings import *
from macro import *
from decal import *

from math import sin, cos


class Bullet(pygame.sprite.Sprite):
    """
    Super class for bullets
    """

    def __init__(self, x, y, angle, speed):
        pygame.sprite.Sprite.__init__(self)

        self.decalType = None
        self.steps = False

        self.cx = x
        self.cy = y
        self.distance = 0
        self.speed = speed*sizeMultiplier

        # Generate bullet vector
        self.xvel = self.speed * cos(angle)
        self.yvel = self.speed * sin(angle)

        self.rect = Rect(x, y, 16*sizeMultiplier, 16*sizeMultiplier)

    def checkCollision(self, room):
        """
        check if bullet hit a platform
        returns true if everything is okay
        """

        if self.steps:
            solids = room.platforms + room.steps
        else:
            solids = room.platforms

        for p in solids:
            if sprite.collide_rect(self, p):
                if not isinstance(p, Step):

                    # Add decal
                    if self.decalType:
                        room.decals.append(Decal(self.rect.midbottom, self.decalType))

                    # Destroy bullet
                    room.bullets.remove(self)
                    return False

        return True

    def move(self):
        """
        Apply bullet vector
        """

        self.distance += self.speed

        self.rect.x += self.xvel
        self.rect.y += self.yvel

    def update(self, room):
        """
        update coordinates of bullet
        """

        self.move()
        self.checkCollision(room)


class PlayerBullet(Bullet):
    """
    Musical notes, subclass of Bullet
    """

    def __init__(self, cx, cy, angle, player):
        Bullet.__init__(self, cx, cy, angle, player.speed)

        # Set default sprite
        self.rect = Rect(cx, cy, (player.damage)*4*sizeMultiplier, 16*sizeMultiplier)
        self.image = Surface(((player.damage)*4, 16))
        self.image.fill(Color("#FFFFFF"))
        self.image.set_colorkey(Color("#FFFFFF"))

        # Generate sprite
        note = pygame.image.load("./assets/sprites/noteshaft0.png").convert()
        note.set_colorkey(Color("#FFFFFF"))

        start = 0
        i = start
        end = (player.damage-1)*4
        while i <= end:
            self.image.blit(note, (i, random.randint(-6, 0)))
            i += 4

        beam = pygame.image.load("./assets/sprites/notebeam0.png").convert()

        i = 0
        end = (4-(player.rate/5))*2
        while i <= end:
            self.image.blit(beam, (start + 3, i))
            i += 2

        self.image.set_colorkey(Color("#FFFFFF"))

        self.image = pygame.transform.scale(self.image, ((player.damage)*4*sizeMultiplier, 16*sizeMultiplier))

        # Bullet position
        self.range = player.range

        # Bullet stats
        self.damage = player.damage

    def update(self, room):
        """
        Update position of note
        """

        self.move()
        if not self.checkCollision(room):
            return

        # Bullet has gone to far
        if self.distance > self.range:
            # Destroy
            room.bullets.remove(self)
            return

        # Check if bullet hit an enemy
        for e in room.enemies:
            if sprite.collide_rect(self, e):

                # Damage enemy
                e.hit()
                e.life -= self.damage

                # Destroy bullet
                room.bullets.remove(self)
                return


class SlugSlime(Bullet):
    """
    Slime boss shoots SlugSlime, subclass of Bullet
    """

    def __init__(self, cx, cy, angle, player, speed):
        Bullet.__init__(self, cx, cy, angle, speed)

        # Init
        self.right = (self.xvel > 0)
        self.player = player
        self.damage = 0.5
        self.decalType = "decal_slime"
        self.steps = True

        # Graphics
        self.rect = Rect(cx, cy, size, size)
        self.anim = Animation("slime", "#000000")
        self.image = self.anim.next(self.right)

    def update(self, room):
        """
        Update position of slime
        """

        self.move()

        if not self.checkCollision(room):
            return

        self.image = self.anim.next(self.right)

        # Apply air friction and gravity
        self.xvel -= 0.1
        self.yvel += 0.5

        # Check if bullet hit player
        if sprite.collide_rect(self, self.player):

            # Damage player
            if not self.player.hit:
                self.player.hit = True
                self.player.life -= self.damage

            # Destroy bullet
            room.bullets.remove(self)
            return