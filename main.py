import pygame
from pygame import *
from game import *
from macro import *

from menu import *
from intro import *
from credits import *
from settings import *


def main():
    """
    Main game function. Called upon starting the game.
    """

    # Init pygame
    pygame.init()
    pygame.mixer.init()

    # Init display
    screen = display.set_mode(DISPLAY)
    pygame.display.set_icon(pygame.image.load("assets/sprites/luridiformis.png").convert())

    display.set_caption("Fungus Time")

    # Create background surfaces
    background = pygame.Surface(DISPLAY)
    background = background.convert()
    background.fill((0, 0, 0))

    # Init timer
    timer = time.Clock()

    playerAlive = True

    """
    This is the main loop for the whole framework
    """

    while 1:

        """
        Black prescreen if I want to record intro and gameplay
        """

        record = False
        while record:
            for e in pygame.event.get():
                # Press a key
                if e.type == KEYDOWN:
                    if e.key == K_z:
                        record = False


        """
        Intro
        """

        # Init the intro
        intro = Intro()

        if playerAlive:
            # Load and play intro narration
            narration = pygame.mixer.Sound("assets/sound/narration.wav")
            narration.play()

            introDisplay = True

        if not introDisplay:
            # Showcase purposes
            narration.stop()

        # While loop for intro
        while introDisplay and playerAlive:
            # Make sure framerate is capped at 60fps
            timer.tick(60)

            screen.blit(background, (0, 0))

            # Handle keyevents
            for e in pygame.event.get():
                checkQuit(e)

                # Skip intro with space
                if e.type == KEYDOWN:
                    if e.key == K_SPACE:
                        introDisplay = False
                        narration.stop()

            # Draw intro slide
            dirty = intro.draw(screen)

            pygame.display.update()

            if intro.finished:
                introDisplay = False # or break

        """
        Menu
        """

        # Load and play menu music
        if playerAlive:
            playMusic("DST-DarkSeraphim")
        else:
            playMusic("DST-Petaluna")

        # Init the menu
        menu = Menu()

        menuDisplay = True

        # While loop for menu
        while menuDisplay:
            # Make sure framerate is capped at 60fps
            timer.tick(60)

            # Handle keyevents
            for e in pygame.event.get():
                checkQuit(e)

                # Handle input
                if menu.handleEvent(e):
                    menuDisplay = False # or break

                # Start game with space
                if e.type == KEYDOWN:
                    if e.key == K_SPACE:
                        menuDisplay = False

            # Draw menu objects
            dirty = menu.draw(screen, playerAlive)

            # Update display
            if dirtOpt:
                # Use dirty rectangles
                pygame.display.update(dirty)

            else:
                pygame.display.update()

        """
        Game
        """

        # Init the game
        game = Game()

        # Load and play menu music
        if not playerAlive:
            playMusic("DST-DarkSeraphim")

        playerAlive = True

        # Main loop for game
        while playerAlive:
            # Make sure framerate is capped at 60fps
            timer.tick(60)

            # Handle keyevents
            for e in pygame.event.get():
                checkQuit(e)

                # Handle key
                game.handleKey(e)

            # Update game objects
            playerAlive = game.update()

            # Draw game objects
            dirty = game.draw(screen)

            # Update display
            if dirtOpt:
                # Use dirty rectangles
                pygame.display.update(dirty)

            else:
                pygame.display.update()

        """
        Game over screen
        """
        game_over_running = True
        game_over_bg = loadSprite("assets/bg/game_over")

        while game_over_running and not game.credits:
            # Make sure framerate is capped at 60fps
            timer.tick(60)

            screen.blit(background, (0, 0))
            screen.blit(game_over_bg, (0, 0))

            # Handle keyevents
            for e in pygame.event.get():
                checkQuit(e)

                # Skip credits with space
                if e.type == KEYDOWN:
                    if e.key == K_SPACE:
                        game_over_running = False

            pygame.display.update()

        """
        Credits
        """
        credits_running = True
        c = Credits()

        while credits_running and game.credits:
            # Make sure framerate is capped at 60fps
            timer.tick(60)

            screen.blit(background, (0, 0))

            credits_running = c.draw(screen)

            # Handle keyevents
            for e in pygame.event.get():
                checkQuit(e)

                # Skip credits with space
                if e.type == KEYDOWN:
                    if e.key == K_SPACE:
                        credits_running = False

            pygame.display.update()


def checkQuit(e):
    """
    Quits if given event is QUIT or ESCAPE
    """

    if e.type == QUIT:
        raise SystemExit("QUIT")

    if e.type == KEYDOWN:
        if e.key == K_ESCAPE:
            raise SystemExit("Quit")


if(__name__ == "__main__"):
    main()
