import pygame
from pygame import *
from macro import *
from threading import Timer
import glob


class IntroSlide:
    """
    Contains and handles all images of a story slide
    """

    def __init__(self, name, speed, loop, length, backgroundslide=False, movement=(0, 0), colorkey=Color("#000000")):

        # Load animation
        self.frames = [loadSprite(frame[:-4], True, colorkey) for frame in sorted(glob.glob("./assets/bg/" + name + "_*.png"))]

        # Load background slide
        if backgroundslide:
            # Speed x, y direction
            self.sx, self.sy = movement

            # Position of background
            self.x, self.y = (0, 0)

            self.background = loadSprite("assets/bg/bg_" + name)

        else:
            self.background = None

        # Init
        self.size = self.frames[0].get_size()

        self.frame = 0
        self.counter = 1

        self.speed = speed
        self.loop = loop

        self.started = False
        self.end = False
        self.finished = False
        self.fade = False

        self.timer = Timer(length, self.timerEnd)
        self.fadeTimer = Timer(length-0.5, self.fadeStart)

    def next(self):
        """
        Return next frame in intro slide
        """

        # Advance frame index
        if not self.end and len(self.frames) > 1:
            if self.counter % self.speed == 0:
                self.frame += 1

                # if self.background:
                #     self.x += 1

                if self.frame > len(self.frames) -1:
                    if self.loop:
                        self.frame = 0
                        self.counter = 0
                    else:
                        self.end = True
                        self.frame -= 1

        self.counter += 1

        # Create composite of current frame
        composite = Surface(self.size)

        if self.background:
            # Moving background
            composite.blit(self.background, (self.x, self.y))

            self.x += self.sx
            self.y += self.sy

        composite.blit(self.frames[self.frame], (0, 0))

        return composite

    def start(self):
        """
        Start the timer
        """

        self.timer.start()
        self.fadeTimer.start()
        self.started = True

    def timerEnd(self):
        """
        Timer calls this function when finished
        """

        self.finished = True

    def fadeStart(self):
        """
        Timer calls this function when slide is ready to fade
        """

        self.fade = True

