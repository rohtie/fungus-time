from pygame import *
import pygame
from settings import *
from macro import *
import os


class UI:
    """
    This class generates a user interface which shows current player status
    """

    def __init__(self, w, h):
        self.bg = Surface((w, h))
        self.bg.fill((0, 0, 0))
        self.image = Surface((w, h))

        # Load consumable icons
        self.key = loadSprite("cons_key")
        self.money = loadSprite("cons_money")

        # Load heart icons
        self.heart = loadSprite("heart")
        self.heartHalf = loadSprite("hearthalf")
        self.heartEmpty = loadSprite("heart_empty")

        # Load default font
        pygame.font.init()
        self.font = pygame.font.Font(os.path.abspath("assets/fonts/Karmatic-Arcade-by-Vic-Fieger.ttf"), 16)

    def update(self, player):
        """
        Update the UI
        """

        self.image.blit(self.bg, (0, 0))

        life = player.life
        lives = player.lives

        # Show full hearts
        for i in range(0, int(life)):
            self.image.blit(self.heart, (size*i + i*2, 0))

        # Add half-heart if needed
        if int(life) != life:
            life = int(life)
            self.image.blit(self.heartHalf, (size*life + life*2, 0))
            life += 1

        # Draw empty hearts
        for i in range(int(life), lives):
            self.image.blit(self.heartEmpty, (size*i + i*2, 0))

        # Draw current amount of keys and money
        self.image.blit(self.font.render("%04d" % (player.money), 0, Color("#FFFFFF")), (15, 65))
        self.image.blit(self.money, (97, 64))

        self.image.blit(self.font.render("%02d" % (player.keys), 0, Color("#FFFFFF")), (148, 65))
        self.image.blit(self.key, (190, 68))


        return image