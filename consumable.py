import pygame
from pygame import *
import random
from shadow import *
from macro import *


class Consumable(pygame.sprite.Sprite):
    """
    Super class for consumable objects
    """

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.rect = Rect(0, 0, 32, 32)

        self.onGround = False
        self.yvel = random.uniform(3, 7) * -2
        self.xvel = random.uniform(-10, 10)

        self.shadow = Shadow("cons_shadow")

    def update(self, solidObjects):
        """
        make consumable fall until it is on ground
        """

        # Only calculate physics if not landed
        if not self.onGround:
            self.yvel += 0.3*sizeMultiplier

            if self.yvel > 30*sizeMultiplier:
                self.yvel = 30*sizeMultiplier

            if self.xvel > -1 and self.xvel < 1:
                self.xvel = 0
            else:
                if self.xvel > 0:
                    self.xvel -= 0.1
                else:
                    self.xvel += 0.1

                self.rect.left += self.xvel

            self.rect.top += self.yvel
            self.onGround = False

            # Check collision with platforms
            for solid in solidObjects:
                if sprite.collide_rect(self, solid):

                    if self.xvel > 0:
                        self.xvel = 0
                        self.rect.right = solid.rect.left - 1

                    elif self.xvel < 0:
                        self.xvel = 0
                        self.rect.left = solid.rect.right + 1

                    elif self.yvel > 0:
                        self.rect.bottom = solid.rect.top
                        self.onGround = True
                        self.yvel = 0

                    elif self.yvel < 0:
                        self.rect.top = solid.rect.bottom
                        self.yvel = self.yvel * -1

        # Update shadow
        self.shadow.update(self, solidObjects)



class Key(Consumable):
    """
    Key, subclass of Consumable
    """

    def __init__(self, x, y):
        Consumable.__init__(self)

        # Change to key sprite
        self.image = loadSprite("cons_key")
        self.image.set_colorkey(Color("#000000"))
        self.rect.topleft = (x, y)



class Heart(Consumable):
    """
    Heart, subclass of Consumable
    """

    def __init__(self, x, y):
        Consumable.__init__(self)

        # Change to heart sprite
        self.image = loadSprite("cons_heart")
        self.image.set_colorkey(Color("#000000"))
        self.rect.topleft = (x, y)


class Money(Consumable):
    """
    Money, subclass of Consumable
    """

    def __init__(self, x, y):
        Consumable.__init__(self)

        # Change to heart sprite
        self.image = loadSprite("cons_money")
        self.image.set_colorkey(Color("#000000"))
        self.rect.topleft = (x, y)
