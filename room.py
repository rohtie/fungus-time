import random
from PIL import Image
from camera import *
from pygame import *
from platform import *
from consumable import *
from item import *
from unlockable import *
from enemy import *
from door import *
from tile import *
from hazards import *
from shadow import *
from portal import *
import pygame
from macro import *
from settings import *
from decal import *

# Enum dict of lists with object types that may be spawned
objectMap = {
15 :[Platform],
44 :[Step],
4  :[Spike],
1  :[None, Money, Money, Money, Heart],
2  :[None, Chest, LockedChest],
13 :[SlugBoss],
7  :[AIwall],
# 6  :[None, Mosquito, Slug, Hopper],
6  :[None, Mosquito, Slug],
10 :[ItemSpawner]
}


class Room(pygame.sprite.Sprite):
    """
    This class takes care of every object in a room and its state.
    """

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)

        self.x = x
        self.y = y

        self.cords = (x, y)

        self.rx = 0
        self.ry = 0

        # Room sprite for minimap
        self.rect = Rect(x*12*sizeMultiplier, y*8*sizeMultiplier, 12*sizeMultiplier, 8*sizeMultiplier)
        self.image = Surface((12*sizeMultiplier, 8*sizeMultiplier))
        self.image.fill(Color("#333333"))

        # Booleans for special rooms
        self.tres = False
        self.bos = False

        # A room contains all the things.
        self.background = loadSprite("assets/bg/filler_", True, None)
        self.platforms = []
        self.portals = []
        self.steps = []
        self.shadows = []
        self.unlockables = []
        self.items = []
        self.itemSpawners = []
        self.consumables = []
        self.enemies = []
        self.doors = []
        self.bullets = []
        self.aiwalls = []
        self.tiles = []
        self.hazards = []
        self.decals = []

        # Common group for all entities that need to be visible on screen
        self.entityGroups = [self.tiles, self.decals, self.shadows, self.unlockables, self.items, self.consumables, self.enemies, self.doors, self.bullets]

        # Room state
        self.visited = False
        self.seen = False
        self.finished = False
        self.locked = False

        # Doorways up right down left
        self.openings = [True, True, True, True]

        # Dirty rectangles
        self.dirty = []
        self.oldDirty = []

        # Portal
        self.portal = None

    def generate(self, xn, yn, blockData):
        """
        Generate room content based on image data
        """

        self.rx = self.ry = xn = yn = 0

        # Load treasure room map
        if self.tres:
            blockData = "./assets/maps/treasure.png"

        # Load boss room map
        if self.bos:
            blockData = "./assets/maps/boss.png"


        # Get image data
        im = Image.open(blockData)

        # Load pixel data, which in this case are color indices
        data = im.load()

        # Generate room based on image data
        w, h = im.size
        for y in range(0, h+2):
            for x in range(0, w+2):
                if x < w and y < h:

                    # spawn object based on pixel index value
                    if data[x, y] != 0:
                        objectList = objectMap[data[x, y]]

                        if len(objectList) > 1:
                            objType = objectList[random.randint(0, len(objectList)-1)]

                        else:
                            objType = objectList[0]

                        if objType:
                            self.addEntity(xn + x + 1, yn + y + 1, objType)

                # Generate boundary around room
                if x == 0 or y == 0 or x == w+1 or y == h+1:
                    if not(((x == w/2 or x == w/2 + 1)) or ((y == h/2 or y == h/2 + 1))):
                        self.addEntity (xn + x, yn + y, Platform )

    def addEntity(self, x, y, t):
        """
        Create a object of type t at position (x, y) and add it to the correct list.
        """

        e = t(x*size, y*size)

        # Append entities to the correct entity groups
        if isinstance(e, Platform):
            if isinstance(e, AIwall):
                self.aiwalls.append(e)

            elif isinstance(e, Step):
                self.steps.append(e)

            else:
                self.platforms.append(e)

        elif isinstance(e, Hazard):
            self.hazards.append(e)

        elif isinstance(e, Consumable):
            self.consumables.append(e)
            self.shadows.append(e.shadow)

        elif isinstance(e, Unlockable):
            self.unlockables.append(e)

        elif isinstance(e, Enemy):
            self.enemies.append(e)
            self.shadows.append(e.shadow)

        elif isinstance(e, Door):
            self.doors.append(e)

        elif isinstance(e, ItemSpawner):
            self.itemSpawners.append(e)

    def treasure(self):
        """
        make room a treasure room
        """

        self.image.fill(Color("#DDDD00"))
        self.tres = True

    def boss(self):
        """
        make room a boss room
        """

        self.image.fill(Color("#770000"))
        self.bos = True

    def genTiles(self, tName):
        """
        Generate tiles for this room based on image data.
        """

        # Load treasure room map
        if self.tres:
            tName = "./assets/maps/treasure_tile.png"

        # Load boss room map
        if self.bos:
            tName = "./assets/maps/boss_tile.png"

        # Load index map
        self.tileIndex = self.generateIndexMap("forest_tiles_map.png")

        # Load tilesheet
        self.tileSheet = loadSprite("forest_tiles")

        # Tile list data
        self.tileImage = Image.open(tName)
        self.tileData = self.tileImage.load()

        # Generate tile list
        self.generateTiles(self.tileData, self.tileIndex, self.tileSheet)

        # Fix tiles where doors are
        self.tileDoorways(self.tileSheet, self.tileIndex)

    def generateIndexMap(self, name):
        """
        Generate and return a dict which can be used to convert between pixel indices to tilesheet coordinates.
        """

        indexMap = {}

        # Load image
        im = Image.open("./assets/maps/" + name)
        data = im.load()

        # Parse
        width, height = im.size
        for y in range(height):
            for x in range(width):
                indexMap[data[x, y]] = (x*size, y*size)

        return indexMap

    def generateTiles(self, data, indexMap, tilesheet):
        """
        Go through image data and convert it to tile objects.
        """

        w, h = (32, 25)
        for y in range(0, h):
            for x in range(0, w):
                if data[x, y] != 0:
                    self.addTile(tilesheet, indexMap[data[x, y]], x, y)

    def tileDoorways(self, tilesheet, indexMap):
        """
        Generate tiles around the doorways.
        """

        up, right, down, left = self.openings

        # get correct tiles
        upWall = indexMap[54]
        downLeaf = indexMap[34]
        downMiddleLeaf = indexMap[17]

        rightSlope = indexMap[53]
        rightWall = indexMap[4]
        leftSlope = indexMap[55]
        leftWall = indexMap[10]

        leftStump = indexMap[42]
        rightStump = indexMap[44]
        leftPreLeaf = indexMap[33]
        rightPreLeaf = indexMap[35]
        leftMidLeaf = indexMap[16]
        rightMidLeaf = indexMap[18]

        rightLeaf = indexMap[2]
        leftLeaf = indexMap[40]

        black = indexMap[116]

        # Put in black tiles to mask out background
        for y in list(range(-1, 10)) + list(range(14, 24)):
            self.addTile(tilesheet, black, -1, y)
            self.addTile(tilesheet, black, 32, y)

        for x in list(range(0, 14)) + list(range(17, 31)):
            self.addTile(tilesheet, black, x, -1)


        # Add tiles at correct place
        if up:
            self.addTile(tilesheet, rightWall, 16, -1)
            self.addTile(tilesheet, leftWall, 15, -1)
            self.addTile(tilesheet, rightSlope, 17, 0)
            self.addTile(tilesheet, leftSlope, 14, 0)
            self.addTile(tilesheet, black, 14, -1)
            self.addTile(tilesheet, black, 18, -1)

        else:
            for x in range(14, 18):
                self.addTile(tilesheet, upWall, x, 0)
                self.addTile(tilesheet, black, x, -1)

        if down:
            self.addTile(tilesheet, leftStump, 15, 24)
            self.addTile(tilesheet, rightStump, 16, 24)
            self.addTile(tilesheet, leftPreLeaf, 17, 23)
            self.addTile(tilesheet, rightPreLeaf, 14, 23)
            self.addTile(tilesheet, leftMidLeaf, 17, 24)
            self.addTile(tilesheet, rightMidLeaf, 14, 24)

        else:
            for x in range(14, 18):
                self.addTile(tilesheet, downLeaf, x, 23)
                self.addTile(tilesheet, downMiddleLeaf, x, 24)

        if left:
            self.addTile(tilesheet, leftWall, 0, 10)
            self.addTile(tilesheet, leftSlope, -1, 11)
            self.addTile(tilesheet, rightPreLeaf, -1, 12)
            self.addTile(tilesheet, rightMidLeaf, -1, 13)
            self.addTile(tilesheet, rightLeaf, 0, 13)
            self.addTile(tilesheet, black, -1, 10)

        else:
            for y in range(10, 14):
                self.addTile(tilesheet, leftWall, 0, y)
                self.addTile(tilesheet, black, -1, y)

        if right:
            self.addTile(tilesheet, rightWall, 31, 10)
            self.addTile(tilesheet, rightSlope, 32, 11)
            self.addTile(tilesheet, leftPreLeaf, 32, 12)
            self.addTile(tilesheet, leftMidLeaf, 32, 13)
            self.addTile(tilesheet, leftLeaf, 31, 13)
            self.addTile(tilesheet, black, 32, 10)
        else:
            for y in range(10, 14):
                self.addTile(tilesheet, rightWall, 31, y)
                self.addTile(tilesheet, black, 32, y)

    def addTile(self, tilesheet, tileCords, x, y):
        """
        Add a new tile at (x, y)
        """

        tx, ty = tileCords
        self.tiles.append(Tile(tilesheet, tx, ty, self.rx + size + x*size, self.ry + size + y*size))

    def addConsumable(self, consumableType, x, y):
        """
        Add new consumable to room
        """

        cons = consumableType(x, y)
        self.consumables.append(cons)
        self.shadows.append(cons.shadow)

    def update(self, player):
        """
        Update all moving objects and control the doors.
        """

        # Close doors if new room
        if not self.finished and (closeDoors or self.bos):
            if (self.rx + 16*sizeMultiplier < player.rect.x < self.rx + size*34 - 16*sizeMultiplier - player.rect.w) and (self.ry + 16*sizeMultiplier < player.rect.y < self.ry + size*24):
                for door in self.doors:
                    door.close()

                self.locked = True

        # Move enemies
        for enemy in self.enemies:

            if not enemy.isDead():
                # Update enemy
                if isinstance(enemy, FlyingEnemy):
                    enemy.update(self.platforms + self.doors)

                elif isinstance(enemy, Hopper):
                    enemy.update(self.platforms + self.doors + self.steps)

                else:
                    enemy.update(self.platforms + self.steps + self.aiwalls + self.doors)

                # AI
                enemy.think(player, self)

            else:
                enemy.image = enemy.anim.next(enemy.right)

            # Normal drop
            if enemy.anim.done and not isinstance(enemy, SlugBoss):

                # Spawn consumable
                if len(self.enemies) == 1:
                    rnd = random.randint(0, 10)
                    if rnd == 0:
                        self.addConsumable(Key, enemy.rect.x, enemy.rect.y)
                    elif 0 < rnd < 3:
                        self.addConsumable(Heart, enemy.rect.x, enemy.rect.y)
                    else:
                        self.addConsumable(Money, enemy.rect.x, enemy.rect.y)

                # Remove object
                self.shadows.remove(enemy.shadow)
                self.enemies.remove(enemy)

            # Boss drop
            elif isinstance(enemy, SlugBoss):

                if enemy.anim.counter == 50:

                    # Explode boss into money
                    x, y = enemy.rect.center
                    for i in range(25):
                        self.addConsumable(Money, x-32, y)

                if enemy.anim.done:

                    # Spawn powerup
                    if player.items:
                        x, y = enemy.rect.center
                        self.items.append(player.items.pop()(x-32, y))

                    # Spawn portal to next level
                    self.portal = Portal(17*size, 6.5*size)

                    # Remove object
                    self.shadows.remove(enemy.shadow)
                    self.enemies.remove(enemy)

        # Move consumables
        for consumable in self.consumables:
            consumable.update(self.platforms + self.doors + self.steps)

        # Make item follow player if picked up
        for item in self.items:
            item.update(player)

        # Move bullets
        for bullet in self.bullets:
            bullet.update(self)

        # Make sure item spawner becomes a item
        for itemSpawner in self.itemSpawners:
            self.itemSpawners.remove(itemSpawner)
            if player.items:
                self.items.append(player.items.pop()(itemSpawner.x, itemSpawner.y))


    def draw(self, screen, camera):
        """
        Draw all objects in the room.

        Returns dirty rectangles
        """

        screen.blit(self.background, (0, 60))

        self.oldDirty = self.dirty

        for entityGroup in self.entityGroups:
            for e in entityGroup:
                rectangle = camera.apply(e)

                screen.blit(e.image, rectangle)

        if self.portal:
            self.portal.update()
            screen.blit(self.portal.image, camera.apply(self.portal))

        self.dirty = []

        # Handle dirty rectangles
        if dirtOpt:
            # Go through tiles
            for tile in self.tiles:
                camera.applyTile(tile)

            # Assume enemies are always dirty if within screenspace
            for enemy in self.enemies:
                enemyRect = camera.apply(enemy)

                if onScreen(enemyRect):
                    self.dirty.append(enemyRect)

        return self.oldDirty + self.dirty

    def check(self):
        """
        Check if room is cleared
        """

        if not self.finished:
            finished = True
            for enemy in self.enemies:
                if not enemy.isDead():
                    finished = False
                    break


            self.finished = finished

            # Enemies are dead, open doors
            if self.finished:
                for door in self.doors:
                    door.open()

                self.locked = False
