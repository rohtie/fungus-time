import pygame
from pygame import *
from macro import *


class Door(pygame.sprite.Sprite):
    """
    Generic door class
    """

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)

        self.image = None
        self.openImage = None
        self.closedImage = None

    def open(self):
        self.image = self.openImage

    def close(self):
        self.image = self.closedImage


class leftDoor(Door):
    """
    Left door, subclass of Door
    """

    def __init__(self, x, y):
        Door.__init__(self)

        self.rect = Rect(x, y, 16*sizeMultiplier, 64*sizeMultiplier)

        self.openImage = loadSprite("door_left_open")
        self.closedImage = loadSprite("door_left")

        self.image = self.openImage


class rightDoor(leftDoor):
    """
    Right door, subclass of Door
    """

    def __init__(self, x, y):
        leftDoor.__init__(self, x, y)

        self.openImage = pygame.transform.flip(self.openImage, True, False)
        self.closedImage = pygame.transform.flip(self.closedImage, True, False)

        self.image = self.openImage

        self.rect.left += 16*sizeMultiplier


class topDoor(Door):
    """
    Top door, subclass of Door
    """

    def __init__(self, x, y):
        Door.__init__(self)

        self.rect = Rect(x, y, 64*sizeMultiplier, 16*sizeMultiplier)

        self.openImage = loadSprite("door_top_open")
        self.closedImage = loadSprite("door_top")

        self.image = self.openImage



class bottomDoor(topDoor):
    """
    Bottom door, subclass of Door
    """

    def __init__(self, x, y):
        topDoor.__init__(self, x, y)

        self.openImage = pygame.transform.flip(self.openImage, False, True)
        self.closedImage = pygame.transform.flip(self.closedImage, False, True)

        self.image = self.openImage

        self.rect.top += 16*sizeMultiplier
